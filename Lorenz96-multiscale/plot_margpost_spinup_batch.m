% batch job

job=batch('plot_margpost_spinup','Pool',20);

wait(job);
diary(job,'plot_margpost_spinup_diary');
load(job);

delete(job);
clear job;