% plot some solution paths

clear;
addpath('~/Documents/MATLAB/tight_subplot/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2017);
RandStream.setGlobalStream(seed);

% settings
K=36; J=10; dim_sys=(1+J)*K;
ord_momts=1:2; cmp_thld=K;
% parameters for Euler-Maruyama discretization of the ODE
N=1e6; T=N.^(2/3); dt=T./N;

% define ODE
ODE.K=K; ODE.J=J; ODE.dim=dim_sys;
ODE.T=T; ODE.N=N;
ODE.eveq=false; % evaluation at equally spaced points
% simulate data
theta0=[10;1;10;10];
dim_par=length(theta0);
ODE.model=@(t,X)F(X(1:ODE.K),X(1+ODE.K:end),theta0); ODE.init=randn(ODE.dim,1);
% random update
parameter=theta0;
% parameter=randn(dim_par,1);
% parameter(1)=8+parameter(1);
% parameter(3)=.4*abs(parameter(3));
% parameter(4)=5+parameter(1);
ODE.model=@(t,X)F(X(1:ODE.K),X(1+ODE.K:end),parameter);

% setting for solution
ord=ord_momts;cmp=1:cmp_thld;
% number of components
k=length(cmp);
% components' position in the whole vector
pos_f=ODE.K+((1:ODE.J)'+(cmp-1)*ODE.J);
pos=[cmp(:);pos_f(:)];
% solve SDE
T_cur=0;T_intvl=1e2;evpt_intvl=0:ODE.T/ODE.N:T_intvl;
sol=ode15s(ODE.model,[T_cur,T_cur+T_intvl],ODE.init);
if ODE.eveq
    ev_x=T_cur+evpt_intvl;
    if T_cur+T_intvl>ODE.T
        ev_x=ev_x(ev_x<=ODE.T);
    end
    ev_y=deval(sol,ev_x,pos);
else
    ev_x=sol.x;
    ev_y=sol.y(pos,:);
end
% get empirical moments
est_momts=update_momt([],ev_x,phi(ev_y,k,ODE.J,ord),ODE.eveq);
T_cur=T_cur+T_intvl;

% plot
fig=figure(1); clf(fig);
set(fig,'pos',[0 800 900 600]);
% ha=tight_subplot(2,2,[.1,.05],[.12,.06],[.05,.05]);
% subplot(2,2,[1,2]);
% plot(sol.x,sol.y);
% xlabel('time','fontsize',14);
% % ylabel('x_t','fontsize',14,'rotation',0);
% ylabel('solutions','fontsize',14);

% record results
relerr=[]; h_est=[];

% solve ODE sequentially
tic;
while T_cur<ODE.T
    N_intvl=length(sol.x);
    sol=odextend(sol,[],min([T_cur+T_intvl,ODE.T]));
    % delete previous history to release space
    sol.x(1:N_intvl-1)=[]; sol.y(:,1:N_intvl-1)=[];
    sol.idata.kvec(1:N_intvl-1)=[]; sol.idata.dif3d(:,:,1:N_intvl-1)=[];
    if ODE.eveq
        ev_x=T_cur+evpt_intvl;
        if T_cur+T_intvl>ODE.T
            ev_x=ev_x(ev_x<=ODE.T);
        end
        ev_y=deval(sol,ev_x,pos);
    else
        ev_x=sol.x;
        ev_y=sol.y(pos,:);
    end
    est_momts_=est_momts;
    % update empirical estimates of mean and covariance
    est_momts=update_momt(est_momts,ev_x,phi(ev_y,k,ODE.J,ord),ODE.eveq);
    T_cur=T_cur+T_intvl;
    % update plot
    subplot(2,2,[1,2]);
    plot(sol.x,sol.y);
    xlabel('time','fontsize',14);
    ylabel('solutions','fontsize',14);
    % calculate relative error and update plot
    relerr_cur=norm(est_momts-est_momts_)./norm(est_momts_);
    fprintf('Relative error in estimated moments: %.8f\n',relerr_cur);
    relerr=[relerr;relerr_cur];
    subplot(2,2,3);
    semilogy(2*T_intvl:T_intvl:T_cur,relerr); grid;
    xlabel('time','fontsize',14);
    ylabel('relative error in moments','fontsize',14);
    % estimate h = <Y2_bar>/<XY_bar>*J
    if any(ord==2)
        XY_bar_avg=mean(est_momts(end-2*k+1:end-k));
        Y2_bar_avg=mean(est_momts(end-k+1:end));
        h_est_cur=Y2_bar_avg/XY_bar_avg*ODE.J;
        fprintf('Estimated h: %.4f; differing by %.4f from the truth.\n',h_est_cur,h_est_cur-parameter(2));
        h_est=[h_est;h_est_cur];
        subplot(2,2,4);
        semilogy(2*T_intvl:T_intvl:T_cur,h_est); grid; hold on;
        line([2*T_intvl,T_cur],parameter(2).*ones(1,2),'linewidth',2,'color','red'); hold off;
        xlabel('time','fontsize',14);
        ylabel('estimated h','fontsize',14);
    end
    % update plot now
    drawnow;
end
time=toc;
fprintf('It took %.2f seconds to calculate the posteriors.\n',time);

% save the picture
% fig.PaperPositionMode = 'auto';
% print(fig,['./result/solutionpaths_N',num2str(N,1)],'-dpng','-r0');
