% plot the strong correlation between h and c

clear;
addpath('~/Documents/MATLAB/tight_subplot/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2017);
RandStream.setGlobalStream(seed);

% settings
K=36; J=10; dim_sys=(1+J)*K;
ord_momts=1:2; cmp_thld=K;
% parameters for Euler-Maruyama discretization of the ODE
N=6e3; T=N.^(2/3); dt=T./N;

% define ODE
ODE.K=K; ODE.J=J; ODE.dim=dim_sys;
ODE.T=T; ODE.N=N;
ODE.eveq=false; % evaluation at equally spaced points
% simulate data
theta0=[10;1;10;10];
dim_par=length(theta0);
ODE.model=@(t,X)F(X(1:ODE.K),X(1+ODE.K:end),theta0); ODE.init=randn(ODE.dim,1);

parameter=theta0;
% parameter=randn(dim_par,1);
% parameter([1,4])=10+parameter([1,4]);
% parameter(2)=abs(parameter(2));
% parameter(3)=5+abs(parameter(3));
% ODE.model=@(t,X)F(X(1:ODE.K),X(1+ODE.K:end),parameter);

% settings
r=[1;.5;.1];
L_r=length(r);
% record results
h_est=zeros(ceil(ODE.T/1e2)-1,L_r);

for i=1:L_r
    
    % update parameter c
    parameter(3)=r(i)*parameter(3);
    ODE.model=@(t,X)F(X(1:ODE.K),X(1+ODE.K:end),parameter);

    % setting for solution
    ord=ord_momts;cmp=1:cmp_thld;
    % number of components
    k=length(cmp);
    % components' position in the whole vector
    pos_f=ODE.K+((1:ODE.J)'+(cmp-1)*ODE.J);
    pos=[cmp(:);pos_f(:)];
    % solve SDE
    T_cur=0;T_intvl=1e2;evpt_intvl=0:ODE.T/ODE.N:T_intvl;
    sol=ode15s(ODE.model,[T_cur,T_cur+T_intvl],ODE.init);
    if ODE.eveq
        ev_x=T_cur+evpt_intvl;
        if T_cur+T_intvl>ODE.T
            ev_x=ev_x(ev_x<=ODE.T);
        end
        ev_y=deval(sol,ev_x,pos);
    else
        ev_x=sol.x;
        ev_y=sol.y(pos,:);
    end
    % get empirical moments
    est_momts=update_momt([],ev_x,phi(ev_y,k,ODE.J,ord),ODE.eveq);
    T_cur=T_cur+T_intvl;

    % record results
    h_est_i=[];

    % solve ODE sequentially
    tic;
    while T_cur<ODE.T
        N_intvl=length(sol.x);
        sol=odextend(sol,[],min([T_cur+T_intvl,ODE.T]));
        % delete previous history to release space
        sol.x(1:N_intvl-1)=[]; sol.y(:,1:N_intvl-1)=[];
        sol.idata.kvec(1:N_intvl-1)=[]; sol.idata.dif3d(:,:,1:N_intvl-1)=[];
        if ODE.eveq
            ev_x=T_cur+evpt_intvl;
            if T_cur+T_intvl>ODE.T
                ev_x=ev_x(ev_x<=ODE.T);
            end
            ev_y=deval(sol,ev_x,pos);
        else
            ev_x=sol.x;
            ev_y=sol.y(pos,:);
        end
        est_momts_=est_momts;
        % update empirical estimates of mean and covariance
        est_momts=update_momt(est_momts,ev_x,phi(ev_y,k,ODE.J,ord),ODE.eveq);
        T_cur=T_cur+T_intvl;
        % calculate relative error and update plot
        relerr_cur=norm(est_momts-est_momts_)./norm(est_momts_);
        fprintf('Relative error in estimated moments: %.8f\n',relerr_cur);
        % estimate h = <Y2_bar>/<XY_bar>*J
        if any(ord==2)
            XY_bar_avg=mean(est_momts(end-2*k+1:end-k));
            Y2_bar_avg=mean(est_momts(end-k+1:end));
            h_est_cur=Y2_bar_avg/XY_bar_avg*ODE.J;
            fprintf('Estimated h: %.4f; differing by %.4f from the truth.\n',h_est_cur,h_est_cur-parameter(2));
            h_est_i=[h_est_i;h_est_cur];
        end
    end
    h_est(:,i)=h_est_i;
    time=toc;
    fprintf('It took %.2f seconds to calculate the posteriors.\n',time);
end


% plot
fig=figure(1); clf(fig);
set(fig,'pos',[0 800 600 350]);
lty={'-','--','-.',':'};
for i=1:L_r
    semilogy(2*T_intvl:T_intvl:T_cur,h_est(:,i),'linewidth',1.2,'linestyle',lty{i},'displayname',sprintf('c=%.1f',r(i)*theta0(2))); grid; hold on;
end
line([2*T_intvl,T_cur],parameter(2).*ones(1,2),'linewidth',2,'color','red','displayname','truth'); hold off;
delta=max(abs(h_est(:)-theta0(2)));
ylim(theta0(2)+[-.01,1.01].*delta);
xlabel('time','fontsize',14);
ylabel('h','fontsize',14,'rot',0);
lgd=legend('show','location','northeast');
set(lgd,'fontSize',14)

% save the picture
% fig.PaperPositionMode = 'auto';
% print(fig,['./result/corr_hc_N',num2str(N,1)],'-dpng','-r0');
