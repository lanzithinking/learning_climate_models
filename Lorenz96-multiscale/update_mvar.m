% sequentially update empirical mean and variance

function [cumN_updated,mean_updated,var_updated]=update_mvar(cumN_current,mean_current,var_current,x_newbatch,ord)
mean_updated=[]; var_updated=[];
[dim,batch_sz]=size(x_newbatch);
if cumN_current==0 || isempty(mean_current)
    mean_current=zeros(dim,1);
end
if cumN_current==0 || isempty(var_current)
    var_current=zeros(dim,1);
end
if ~exist('ord','var')
    ord=1;
end

% update cumulative sample size N
cumN_updated=cumN_current+batch_sz;

% update empirical mean
if any(ord==1)
    x_mean=mean(x_newbatch,2);
    mean_updated=(cumN_current.*mean_current+batch_sz.*x_mean)./cumN_updated; % (D,1)
end

% rank-2 update empirical variance
if any(ord==2)
    rk2update=cumN_current*batch_sz/cumN_updated.*(mean_current-x_mean).^2;
    var_updated=((cumN_current-1).*var_current+(batch_sz-1).*var(x_newbatch,0,2)+rk2update)./(cumN_updated-1); % (D,1)
end
