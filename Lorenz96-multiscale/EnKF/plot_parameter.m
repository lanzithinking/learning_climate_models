% This is to plot parameters vs iteration by EnKF

clear;
addpath('~/Documents/MATLAB/boundedline/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2017);
RandStream.setGlobalStream(seed);

% parameters for setting
Nensbl=100;
nzlvl=0.5;

% plot setting
fig=figure(1); clf(fig);
lty={'-','--','-.',':'};
clr={'blue','red'};
marg_name={'F','h','c','b'};

width=7.25; height=width/1.63;
init_figure(height,width);

% plot parameters
% load data
files = dir('./summary');
nfiles = length(files) - 2;
found=false;
for k=1:nfiles
    if ~isempty(strfind(files(k+2).name,['_Nensbl',num2str(Nensbl),'_nz',num2str(nzlvl),'_']))
        load(strcat('./summary/', files(k+2).name));
        fprintf('%s loaded.\n',files(k+2).name);
        found=true; break;
    end
end
if found
    % calculate 25% and 75% quantile
    quant=quantile(ensbl_last,[.25,.75],2);
    % plot parameter
%     [l,p]=boundedline((1:n_max)',ensbl_mean([2,1,3,4],:)',reshape(ensbl_std([2,1,3,4],:)',[size(ensbl_std,2),1,size(ensbl_std,1)]),'alpha'); hold on;
    [l,p]=boundedline((1:n_max)',ensbl_mean',abs(permute(quant,[3,2,1])-reshape(ensbl_mean',size(ensbl_mean,2),1,[])),'transparency',0.7); hold on;
%     outlinebounds(l,p);
    hline=refline(zeros(length(theta0),1),theta0);
    hline.set('color','black','linewidth',2);
    set(gca,'box','on','fontsize',18,'box','off','tickdir','out');
    xlabel('Iteration','fontsize',20);ylabel('Parameters','fontsize',20);
%     title(['M=',num2str(Nensbl),' - r=',num2str(nzlvl)],'fontsize',20);
    lgnd=legend(marg_name,'location','best','box','off');
    set(lgnd,'fontsize',20);
end
% save plot
fig.PaperPositionMode = 'auto';
print(fig,'./summary/parameter','-dpng','-r0');