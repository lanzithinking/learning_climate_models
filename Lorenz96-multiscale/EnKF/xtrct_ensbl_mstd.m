% This is to extract EnKF ensemble mean and standard deviation (last iteration)

clear;
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2017);
RandStream.setGlobalStream(seed);

% parameters for setting
Nensbl=[10,100];
L_ensbl=length(Nensbl);
nzlvl=[0.1,0.2,0.5,0.5555,1];
L_nzlvl=length(nzlvl);

% save optimal solutions
ensbl_last_mean=zeros(L_nzlvl*L_ensbl,4);
ensbl_last_med=zeros(L_nzlvl*L_ensbl,4);
ensbl_last_std=zeros(L_nzlvl*L_ensbl,4);
lbl_ensbl=zeros(L_nzlvl*L_ensbl,1);
lbl_nzlvl=zeros(L_nzlvl*L_ensbl,1);

% load data
files = dir('./summary');
nfiles = length(files) - 2;
for i=1:L_ensbl
    for j=1:L_nzlvl
        found=false;
        for k=1:nfiles
            if ~isempty(strfind(files(k+2).name,['_Nensbl',num2str(Nensbl(i)),'_nz',num2str(nzlvl(j)),'_']))
                load(strcat('./summary/', files(k+2).name));
                fprintf('%s loaded.\n',files(k+2).name);
                found=true; break;
            end
        end
        if found
            % take the last ensemble
            ensemble=ensbl_last(:,:,end);
            % save the optimal solution with minimal error
            ensbl_last_mean((i-1)*L_nzlvl+j,:)=mean(ensemble,2);
            ensbl_last_med((i-1)*L_nzlvl+j,:)=median(ensemble,2);
            ensbl_last_std((i-1)*L_nzlvl+j,:)=std(ensemble,0,2);
            lbl_ensbl((i-1)*L_nzlvl+j)=Nensbl(i);
            lbl_nzlvl((i-1)*L_nzlvl+j)=nzlvl(j);
        end
    end
end
% save to csv file
csvwrite('./summary/ensbl_mstd.csv',[lbl_ensbl,lbl_nzlvl,ensbl_last_mean,ensbl_last_med,ensbl_last_std]);