% This is to plot errors of results by EnKF

clear;
addpath('../','~/Documents/MATLAB/tight_subplot/','~/Documents/MATLAB/columnlegend/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2017);
RandStream.setGlobalStream(seed);

% parameters for setting
Nensbl=[10,100];
L_ensbl=length(Nensbl);
nzlvl=[0.1,0.2,0.5,1.0];
L_nzlvl=length(nzlvl);

% plot setting
% fig1=figure(1); clf(fig1);
fig2=figure(2); clf(fig2);
lty={'-','--','-.',':'};
clr={'blue','red'};

width=7.25; height=width/1.63;
init_figure(height,width);
% ha=tight_subplot(1,1,[0.025,0],[.14,.1],[.07,.17]);

% plot errors
% h_sub=subplot(ha(1));
% load data
files = dir('./summary');
nfiles = length(files) - 2;
for i=1:L_ensbl
    if i==2
%         set(0,'currentfigure',fig1);
%         ax = gca;
%         ax.ColorOrderIndex = 1;
%         set(0,'currentfigure',fig2);
        ax = gca;
        ax.ColorOrderIndex = 1;
    end
    for j=1:L_nzlvl
        found=false;
        for k=1:nfiles
            if ~isempty(strfind(files(k+2).name,['_Nensbl',num2str(Nensbl(i)),'_nz',num2str(nzlvl(j)),'_']))
                load(strcat('./summary/', files(k+2).name));
                fprintf('%s loaded.\n',files(k+2).name);
                found=true; break;
            end
        end
        if found
            % plot errors
%             set(0,'currentfigure',fig1);
%             semilogy(err.*nzlvl(j),'linestyle',lty{mod((-1)^(i-1)*j,length(lty)+1)},'linewidth',1.5,'displayname',['M=',num2str(Nensbl(i)),' - r=',num2str(nzlvl(j),'%.1f')]); hold on;
%             set(0,'currentfigure',fig2);
            plot(sqrt(sum(([10,1,10,10]-ensbl_mean').^2,2)),'linestyle',lty{mod(i,L_ensbl)+1},'linewidth',1.5,'displayname',['M=',num2str(Nensbl(i)),' - r=',num2str(nzlvl(j),'%.1f')]); hold on;
%             lgd_str{(i-1)*L_nzlvl+j}=['M=',num2str(Nensbl(i)),' - r=',num2str(nzlvl(j),'%.1f')];
        end
    end
end
% set(0,'currentfigure',fig1);
% set(gca,'fontsize',14);
% xlabel('Iteration','fontsize',16);ylabel('Error (misfit)','fontsize',16);
% h_lgd=legend('show','location','best');
% set(h_lgd,'fontsize',14);
% set(0,'currentfigure',fig2);
set(gca,'fontsize',18,'box','off','tickdir','out');
xlabel('Iteration','fontsize',20);ylabel('Error (2norm)','fontsize',20);
% ylim([0,15]);
% h_lgd=legend('show','location','northeastoutside');
% % h_lgd=columnlegend(2,[[repmat(['M= ',num2str(Nensbl(1))],L_nzlvl,1),repmat(' -r=',L_nzlvl,1),num2str(nzlvl','%.1f')];...
% %                       [repmat(['M=',num2str(Nensbl(2))],L_nzlvl,1),repmat(' -r=',L_nzlvl,1),num2str(nzlvl','%.1f')]]);
% % h_lgd=columnlegend(2,lgd_str);
% h_lgd=legend(lgd_str);
% h_pos=h_sub.Position;
% set(h_lgd,'fontsize',12,'box','off');
% set(h_lgd,'position',[sum(h_pos([1,3]))+.06 h_pos(2) 0.05 h_pos(4).*1.02]);
% save plot
% fig1.PaperPositionMode = 'auto';
% print(fig1,'./summary/err_misfit','-dpng','-r0');
fig2.PaperPositionMode = 'auto';
print(fig2,'./summary/err_2norm','-dpng','-r0');

% % plot parameter error
% fig3=figure(3); clf(fig3);
% plot([10,1,10,10]-parameter_opt','linewidth',1.2);
% set(gca,'fontsize',14);
% xlabel('Iteration','fontsize',16);ylabel('Error (parameter)','fontsize',16);
% title([num2str(Nensbl(i)),' ensbl-nzlvl ',num2str(nzlvl(j))],'fontsize',16);
% legend(['F';'h';'c';'b'],'location','best');
% % save plot
% fig3.PaperPositionMode = 'auto';
% print(fig3,'./summary/err_parameter','-dpng','-r0');