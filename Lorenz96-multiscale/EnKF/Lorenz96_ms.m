% Fitting Parameters of Stochastic Dynamical Systems To Equilibrium
% Statistics example

clear;
addpath('../','../../optimizer/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2017);
RandStream.setGlobalStream(seed);

% settings
K=36; J=10; dim_sys=(1+J)*K;
ord_momts=1:2; cmp_thld=K;
% parameters for Euler-Maruyama discretization of the ODE
% N=1e4; T=N.^(2/3); dt=T./N;
T=100; N=floor(T.^(3/2)); dt=T./N;

% define ODE
ODE.K=K; ODE.J=J; ODE.dim=dim_sys;
ODE.T=T; ODE.N=N;
ODE.eveq=true; % evaluation at equally spaced points
ODE.updtinit=true; % update initialization for the next solution
% ODE.thld=1e-2; % threshold for stopping solving ODE
ODE.spinup=0; % spin up (burn in) some initial solution points
% simulate data
theta0=[10;1;10;10];
dim_par=length(theta0);
ODE.model=@(t,X)F(X(1:ODE.K),X(1+ODE.K:end),theta0); %ODE.init=randn(ODE.dim,1);
load('../sol_T1e4.mat','solT1e4');
ODE.init=solT1e4;
y=g(theta0,ODE,ord_momts);
% define data
data.y=y;
[~,data.var]=get_mvar4phi(theta0,ODE,ord_momts);
r=0.5; % proportion of standard deviation
% data.gamma=sqrt(T).*sqrt(data.var).*r;
data.Gamma=diag(data.var).*r^2;
data.ord=ord_momts;
data.cmp=1:cmp_thld;
% define prior
prior.mean=[10;0;2;5]; % Normal prior for F,h,b
prior.var=[10;1;.1;10]; % log-Normal prior for c
% prior.var=.2.*ones(dim_par,1); % testing
% prior.var(3)=2e-2*prior.var(3);
% prior.mean=1.5.*prior.mean;

% optimization setting
n_max=25; thld=0;
n_ensbl=10;
ptb_nz=false; % whether to add perturbing noise
% set parallel environment
% myCluster=parcluster('local');
% myCluster.NumWorkers=min([20,n_ensbl]);
poolobj=gcp('nocreate');
if isempty(poolobj)
%     poolobj=parpool(myCluster,myCluster.NumWorkers);
    poolobj=parpool('local',min([20,n_ensbl]));
end

% initializatioin
parameter=mvnrnd(prior.mean',diag(prior.var),n_ensbl)';
parameter(3,:)=exp(parameter(3,:));
% define EnKF
enkf=EnKF(parameter,ODE,@(q,ODE)g_T_par(q,ODE,data.ord,data.cmp,poolobj),data,ptb_nz);

% optimize using EnKF
[ensbl_mean,ensbl_std,ensbl_last,err,time]=enkf.run(n_max,thld);

% save result
time_lbl=regexprep(num2str(fix(clock)),'    ','_');
f_name=['EnKF_D',num2str(dim_par),'_N',num2str(N,1),'_Nensbl',num2str(n_ensbl),'_nz',num2str(r),'_',time_lbl,'.mat'];
if exist('result','dir')~=7
    mkdir('result');
end
save(['./result/',f_name],'seed','theta0','ODE','data','r','prior','parameter','n_max','thld','n_ensbl','ptb_nz','ensbl_mean','ensbl_std','ensbl_last','err','time');

% summarize
fprintf('\nIt takes %.2f seconds to run EnKF.\n', time);

% delete the pool
delete(poolobj);