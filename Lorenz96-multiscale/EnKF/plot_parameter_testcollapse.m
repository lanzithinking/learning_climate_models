% This is to plot parameters vs iteration by EnKF to test the collapse of
% ensembles

clear;
addpath('~/Documents/MATLAB/boundedline/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2017);
RandStream.setGlobalStream(seed);

% parameters for setting
Nensbl=100;
nzlvl=0.5;

% plot setting
fig=figure(1); clf(fig);
lty={'-','--','-.',':'};
clr={'blue','red'};
marg_name={'F','h','c','b'};

% plot parameters
% load data
files = dir('./test_collapse');
nfiles = length(files) - 2;
found=false;
for k=1:nfiles
    if ~isempty(strfind(files(k+2).name,['_Nensbl',num2str(Nensbl),'_nz',num2str(nzlvl),'_noptb']))
        load(strcat('./test_collapse/', files(k+2).name));
        fprintf('%s loaded.\n',files(k+2).name);
        found=true; break;
    end
end
if found
    % plot parameter
    [l,p]=boundedline((1:n_max)',ensbl_mean([2,1,3,4],:)',reshape(ensbl_std([2,1,3,4],:)',[size(ensbl_std,2),1,size(ensbl_std,1)]),'alpha');
%     outlinebounds(l,p);
    set(gca,'box','on','fontsize',14);
    xlabel('Iteration','fontsize',16);ylabel('Parameters','fontsize',16);
    title(['M=',num2str(Nensbl),' - p=',num2str(nzlvl)],'fontsize',16);
    legend(marg_name([2,1,3,4]),'location','best');
end
% save plot
fig.PaperPositionMode = 'auto';
print(fig,'./test_collapse/parameter','-dpng','-r0');