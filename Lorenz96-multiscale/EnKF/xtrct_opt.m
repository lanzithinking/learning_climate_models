% This is to extract optimal solutions by EnKF

clear;
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2017);
RandStream.setGlobalStream(seed);

% parameters for setting
Nensbl=[10,100];
L_ensbl=length(Nensbl);
nzlvl=[0.1,0.2,0.5,1];
L_nzlvl=length(nzlvl);

% save optimal solutions
opt_sol=zeros(L_nzlvl*L_ensbl,4);
lbl_ensbl=zeros(L_nzlvl*L_ensbl,1);
lbl_nzlvl=zeros(L_nzlvl*L_ensbl,1);

% load data
files = dir('./summary_0');
nfiles = length(files) - 2;
for i=1:L_ensbl
    for j=1:L_nzlvl
        found=false;
        for k=1:nfiles
            if ~isempty(strfind(files(k+2).name,['_Nensbl',num2str(Nensbl(i)),'_nz',num2str(nzlvl(j)),'_']))
                load(strcat('./summary_0/', files(k+2).name));
                fprintf('%s loaded.\n',files(k+2).name);
                found=true; break;
            end
        end
        if found
            % save the optimal solution with minimal error
            [~,I]=min(err);
            opt_sol((i-1)*L_nzlvl+j,:)=parameter_opt(:,I);
            lbl_ensbl((i-1)*L_nzlvl+j)=Nensbl(i);
            lbl_nzlvl((i-1)*L_nzlvl+j)=nzlvl(j);
        end
    end
end
% save to csv file
csvwrite('./summary_0/opt_sol.csv',[lbl_ensbl,lbl_nzlvl,opt_sol]);