% batch job

job=batch('Lorenz96_ms','Pool',10);

wait(job);
diary(job,'Lorenz96_ms_diary');
load(job);

delete(job);
clear job;