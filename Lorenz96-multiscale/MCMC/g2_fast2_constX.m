% no analytic moments in non-linear case
% use the result of a long simulation instead

function truth=g2_fast2_constX(parameter,ODE,ord)
if ~exist('parameter','var')
    parameter=[1;10;10];
end
if ~exist('ODE','var')
    ODE.K=36; ODE.J=10; ODE.dim=(1+ODE.J)*ODE.K;
    ODE.init=randn(ODE.dim,1); ODE.model=@(t,X)F_fast(X,ODE.init(1:ODE.K),parameter);
    ODE.N=1e5; ODE.T=ODE.N^(2/3);
    ODE.eveq=true; % evaluation at equally spaced points
end
if ~exist('slow','var')
    addpath('../');
    m_phi=get_mvar4phi();
    slow=@(t)m_phi(1:ODE.K);
end
if ~exist('ord','var')
    ord=1:2;
end
% update ODE
ODE.model=@(t,X)F_fast(X,slow(t),parameter);

f_name=['../truth_mvar_K',num2str(ODE.K),'_J',num2str(ODE.J),'_constX.mat'];
if exist(f_name,'file')==2 && isequal(parameter,[1;10;10])
    load(f_name,'m_est');
    truth=m_est;
    if all(ord==1)
        truth=truth(1:ODE.J*ODE.K);
    end
else
    seed = RandStream('mt19937ar','Seed',2017);
    RandStream.setGlobalStream(seed);
    % treat the approximation result based on long time solution as truth
    truth=g_T_fast2(parameter,ODE,slow,ord);
    save(f_name,'truth');
end

end