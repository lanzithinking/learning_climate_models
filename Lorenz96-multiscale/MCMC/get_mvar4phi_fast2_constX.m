% get mean and covariance of moments of fast variables y

function [m_phi,var_phi]=get_mvar4phi_fast2_constX(parameter,ODE,slow,ord)
if ~exist('parameter','var')
    parameter=[1;10;10];
end
if ~exist('ODE','var')
    ODE.K=36; ODE.J=10; ODE.dim=(1+ODE.J)*ODE.K;
    ODE.init=randn(ODE.dim,1); ODE.model=@(t,X)F_fast(X,ODE.init(1:ODE.K),parameter);
    ODE.N=1e4; ODE.T=ODE.N^(2/3);
    ODE.eveq=true; % evaluation at equally spaced points
end
if ~exist('slow','var') || isempty(slow)
    addpath('../');
    m_phi=get_mvar4phi();
    slow=@(t)m_phi(1:ODE.K);
end
if ~exist('ord','var')
    ord=1:2;
end
% update ODE
ODE.model=@(t,X)F_fast(X,slow(t),parameter);

f_name=['~/PROJ-GPS/WeatherForecast/code/Lorenz96-multiscale/truth_mvar_K',num2str(ODE.K),'_J',num2str(ODE.J),'_constX.mat'];
if exist(f_name,'file')==2 && isequal(parameter,[1;10;10])
    load(f_name,'m_est','var_est');
else
    seed = RandStream('mt19937ar','Seed',2017);
    RandStream.setGlobalStream(seed);
    % treat the approximation result based on long time solution as truth
    % solve SDE
    T_cur=0;T_intvl=1e1;evpt_intvl=0:ODE.T/ODE.N:T_intvl;
    sol=ode15s(ODE.model,[T_cur,T_cur+T_intvl],ODE.init(1+ODE.K:end));
    if ODE.eveq
        ev_x=T_cur+evpt_intvl;
        if T_cur+T_intvl>ODE.T
            ev_x=ev_x(ev_x<=ODE.T);
        end
        ev_y=deval(sol,ev_x);
    else
        ev_y=sol.y;
    end
    % initialization
    cumN=0;
    % get empirical mean and variance of phi2
    [cumN,m_est,var_est]=update_mvar(cumN,[],[],phi2(ev_y,ord),ord);
    est_momts=[m_est;var_est];
    T_cur=T_cur+T_intvl;

    % record results
    relerr=[];

    % solve ODE sequentially
    tic;
    while T_cur<ODE.T
        N_intvl=length(sol.x);
        sol=odextend(sol,[],min([T_cur+T_intvl,ODE.T]));
        % delete previous history to release space
        sol.x(1:N_intvl-1)=[]; sol.y(:,1:N_intvl-1)=[];
        sol.idata.kvec(1:N_intvl-1)=[]; sol.idata.dif3d(:,:,1:N_intvl-1)=[];
        if ODE.eveq
            ev_x=T_cur+evpt_intvl;
            if T_cur+T_intvl>ODE.T
                ev_x=ev_x(ev_x<=ODE.T);
            end
            ev_y=deval(sol,ev_x);
        else
            ev_y=sol.y;
        end
        est_momts_=est_momts;
        % update empirical estimates of mean and variance
        [cumN,m_est,var_est]=update_mvar(cumN,m_est,var_est,phi2(ev_y,ord),ord);
        est_momts=[m_est;var_est];
        T_cur=T_cur+T_intvl;
        % calculate relative error
        relerr_cur=norm(est_momts-est_momts_)./norm(est_momts_);
        fprintf('Relative error in estimated moments: %.8f\n',relerr_cur);
        relerr=[relerr;relerr_cur];
    end
    time=toc;
    fprintf('It took %.2f seconds to get the estimates.\n',time);
    % save the results
    save(f_name,'ODE','parameter','m_est','var_est','relerr');
end
% set values
m_phi=m_est;
var_phi=var_est;