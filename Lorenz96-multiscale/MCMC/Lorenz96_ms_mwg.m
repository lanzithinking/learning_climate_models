% Fitting Parameters of Stochastic Dynamical Systems To Equilibrium
% Statistics example
% using Metropolis-Within-Gibbs algorithm

% clear;
addpath('../','../../sampler/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2017);
RandStream.setGlobalStream(seed);

% settings
K=36; J=10; dim_sys=(1+J)*K;
ord_momts=1:2; cmp_thld=K;
% parameters for Euler-Maruyama discretization of the ODE
N=1e3; T=N.^(2/3); dt=T./N;

% define ODE
ODE.K=K; ODE.J=J; ODE.dim=dim_sys;
ODE.T=T; ODE.N=N;
ODE.eveq=false; % evaluation at equally spaced points
% simulate data
theta0=[10;1;10;10];
dim_par=length(theta0);
ODE.model=@(t,X)F(X(1:ODE.K),X(1+ODE.K:end),theta0); ODE.init=randn(ODE.dim,1);
y=g(theta0,ODE,ord_momts);
% define data
data.y=y;
data.gamma=sqrt(T/10);
data.ord=ord_momts;
data.cmp=1:cmp_thld;
% define prior
prior.mean=zeros(dim_par,1); prior.mean(1)=5; prior.mean(3)=1;
% prior.cholC=eye(dim_par);
prior.C=eye(dim_par);

% sampling setting
stepsz=[1e-2,1e-2,1e-2,1e-2]; Nleap=[1,1,1,1];
% alg={'pCN'};
alg={'RWM'};
Nsamp=1e3; burnrate=0.1; thinning=2;
Niter=Nsamp*thinning;
NBurnIn=floor(Niter*burnrate);
Niter=Niter+ NBurnIn;

% initializatioin
parameter=randn(dim_par,1);
parameter([1,4])=8+parameter([1,4]);
parameter(2)=abs(parameter(2));
parameter(3)=4+abs(parameter(3));
% parameter=theta0+.01.*randn(dim_par,1);
% define MCMC
% MCMC=geoinfMC(parameter,prior,@(q,opt)geom(q,prior,data,ODE),alg{1},stepsz,Nleap,[-Inf;-Inf;0;-Inf]);
% MCMC=geoMC(parameter,@(q,opt)geom(q,prior,data,ODE),alg{1},stepsz,Nleap,[-Inf;-Inf;0;-Inf]);
lb=[-Inf;-Inf;0;0]; id=(1:dim_par)';
MCMC=cell(dim_par,1);
for i=1:dim_par
    MCMC{i}=geoMC(parameter(i),@(q,opt)geom(q.*(id==i)+parameter.*(id~=i),prior,data,ODE),alg{1},stepsz(i),Nleap(i),lb(i));
end

% allocation to save
samp=zeros(Nsamp,dim_par);
engy=zeros(Niter,dim_par);
acpt=zeros(1,dim_par); % overall acceptance
accp=zeros(1,dim_par); % online acceptance
acpt_ind=zeros(1,dim_par);

fprintf('Running %s Delta-Spherical HMC...\n',alg_choice);
prog=0.05:0.05:1;
tic;
for iter=1:Niter
    
    % display sampleing progress and online acceptance rate
    if ismember(iter,floor(Niter.*prog))
        fprintf('%.0f%% iterations completed.\n',100*iter/Niter);
        fprintf('Online acceptance rates: %.2f%%, %.2f%%, %.2f%%, %.2f%%\n', accp.*(100/floor(prog(1)*Niter)));
        accp=zeros(1,dim_par);
    end
    
    % use given MCMC to collect samples
    % Metropolis-within-Gibbs
    for i=1:dim_par
        [MCMC{i},acpt_ind(i)]=MCMC{i}.(alg{1})();
        parameter(i)=MCMC{i}.q;
        engy(iter,i)=MCMC{i}.u;
    end
    
    % accptances
    accp=accp+acpt_ind;
    
    % burn-in complete
    if(iter==NBurnIn)
        fprintf('Burn in completed!\n');
    end
    
    % save samples after burn-in
    if(iter>NBurnIn)
        if mod(iter-NBurnIn,thinning) == 0
            samp(ceil((iter-NBurnIn)/thinning),:)=parameter';
        end
        acpt=acpt+acpt_ind;
    end

end

% count time
time=toc;
% save results
acpt=acpt./(Niter-NBurnIn);
% save
time_lbl=regexprep(num2str(fix(clock)),'    ','_');
f_name=[alg{1},'_D',num2str(dim_par),'_',time_lbl];
if exist('result','dir')~=7
    mkdir('result');
end
save(['result/',f_name,'.mat'],'Nsamp','NBurnIn','thinning','stepsz','Nleap','samp','engy','acpt','time');
% append additional data/model information
save(['./result/',f_name],'seed','theta0','ODE','data','prior','parameter','-append');
% rename
if exist(['./result/',f_name,'.mat'],'file')==2
    movefile(['./result/',f_name,'.mat'],['./result/mwg_',f_name,'.mat']);
end

% summarize
fprintf('\nIt takes %.2f seconds to collect %d samples after thinning %d.\n', time,Nsamp,thinning);
fprintf('\nThe final acceptance rate is: %.2f\n\n',acpt);
% efficiency measurement
addpath('./result/');
CalculateStatistics(f_name,'./result/');
% % some plots
% load(['./result/',f_name]);
% fig1=figure(1); set(fig1,'pos',[0 800 900 300]);
% idx=floor(linspace(1,size(samp,1),min([1e4,size(samp,1)])));
% % dim2plot=[1,ceil(dim_par/2),dim_par];
% dim2plot=1:dim_par;
% subplot(1,3,1);
% plot(samp(idx,dim2plot));
% subplot(1,3,2);
% % plotmatrix(samp(idx,dim));
% histfit(samp(idx,dim2plot));
% subplot(1,3,3);
% plot(engy);