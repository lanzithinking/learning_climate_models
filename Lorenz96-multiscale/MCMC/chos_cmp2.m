% retrieve the data of moments for chosen position components from that for
% for all positions of dimension dim.
% -----------------------------------
% Shiwei Lan @ CalTech, 2017
% -----------------------------------
% vector x has dimension dim
% data y is the collection of moments of x
% the number of order r moments: nchoosek(d+r-1,r)
% cmp are the indices of chosen components of x, a subset of 1:dim
% the output are moments corresponding to those chosen components
% ---------------------------------------------------------------


function y_cmp=chos_cmp2(y,dim,ord,cmp)
if ~exist('cmp','var') || max(cmp) > dim
    cmp=1:dim;
end
if size(y,1)==1
    y=y';
end

d=dim;

y_ord=cell(max(ord),1);
idxs_momt=@(d,r)nchoosek(d+r-1,r-1)-1+(1:nchoosek(d+r-1,r));

if any(ord==1)
    y_ord{1}=y(idxs_momt(d,1));
    y_ord{1}=y_ord{1}(cmp);
end
if any(ord==2)
    y_ord{2}=ivech(y(idxs_momt(d,2)));
    y_ord{2}=vech(y_ord{2}(cmp,cmp));
end
if any(ord>2)
    warning('Co-moments of order more than 2 are not available yet!');
end

y_cmp=cell2mat(y_ord);

end
