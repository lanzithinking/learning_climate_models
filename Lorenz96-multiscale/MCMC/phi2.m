% moment function for Lorenz96-multiscale fast system
% particularly, extract Y and Y^2

function momts=phi2(SOL,ord)
addpath('../');
Y=[];Y2=[];
if ~exist('ord','var')
    ord=1:2;
end

dim=size(SOL,1);

if any(ord==1)
    Y=SOL;
end
if any(ord==2)
    SOL2=reshape(SOL',[],dim,1).*reshape(SOL',[],1,dim);
    Y2=SOL2(:,vech(reshape(1:dim^2,dim,dim)))';
end
if any(ord>2)
    warning('Co-moments of order more than 2 are not available yet!');
end

momts=[Y;Y2];

end