function [u,g,eigs,ODE]=geom_fast2(parameter,prior,data,ODE,slow)
u=[]; g=[]; eigs=[];
if ~isfield(data,'cmp') || max(data.cmp) > ODE.K
    data.cmp=1:ODE.K;
end
est=g_T_fast2(parameter,ODE,@(t)deval(slow,t),data.ord,data.cmp);
pos=(1:ODE.J)'+(data.cmp-1)*ODE.J;
y_cmp=chos_cmp2(data.y,ODE.J*ODE.K,data.ord,pos);
gamma_cmp=chos_cmp(data.gamma,ODE.K,data.ord,data.cmp,'fast');
% adjust gamma_cmp
l_cmp=length(data.cmp);
gamma_1=gamma_cmp(1:l_cmp); gamma_2=gamma_cmp(1+l_cmp:end);
gamma_cmp=[kron(gamma_1,ones(ODE.J,1));vech(kron(gamma_2,ones(ODE.J)))];

% parameters: h,c,b
u=ODE.T./2.*sum(((y_cmp-est)./gamma_cmp).^2) + .5*sum(([parameter(1);log(abs(parameter(2)));parameter(3)]-prior.mean).^2./prior.var) + log(abs(parameter(2)));

% update initial value for the next solution
if isfield(ODE,'updtinit') && ODE.updtinit
    updtX=ODE.init(1:ODE.K); updtY=reshape(ODE.init(ODE.K+1:end),ODE.J,ODE.K);
%     l_cmp=length(data.cmp);
%     updtX(data.cmp)=est(1:l_cmp);
    updtY(:,data.cmp)=est(l_cmp+(1:l_cmp));
    ODE.init=[updtX;updtY(:)];
end

end