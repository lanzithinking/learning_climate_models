% plot estimated marginal posterior

clear;
addpath('../','../../tight_subplot/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2017);
RandStream.setGlobalStream(seed);

% truth
theta0=[10;1;10;10];
dim_par=length(theta0);
% define prior
prior.mean=[10;0;2;5]; % Normal prior for F,h,b
prior.var=[10;1;.1;10]; % log-Normal prior for c

% setting
fig=figure(1); clf(fig);
% set(fig,'pos',[0 800 1000 350]);
width=7.25; height=width/1.63;
init_figure(height,width);
ha=tight_subplot(1,dim_par,[0,.09],[.14,.1],[.05,.03]);
lty={'-','--','-.',':'};
marg_name={'F','h','c','b'};

% plot prior and posteriors
% load data
files = dir('./summary');
nfiles = length(files) - 2;
found=false;
for j=1:nfiles
    if ~isempty(strfind(files(j+2).name,['RWM_D',num2str(dim_par)])) && contains(files(j+2).name,'_N1e+3_nz0.5_')
        load(strcat('./summary/', files(j+2).name));
        fprintf('%s loaded.\n',files(j+2).name);
        found=true;
    end
end
if found
    for j=1:dim_par
        subplot(ha(j));
        % plot prior
        if j~=3
            x=linspace(prior.mean(j)-2.*sqrt(prior.var(j)),prior.mean(j)+2.*sqrt(prior.var(j)),100);
            plot(x,normpdf(x,prior.mean(j),sqrt(prior.var(j))),'linewidth',1.5,'displayname','prior'); hold on;
            xlim([-theta0(j),2*theta0(j)]);
        else
            x=linspace(0,2.*exp(prior.mean(j)),100);
            plot(x,lognpdf(x,prior.mean(j),sqrt(prior.var(j))),'linewidth',1.5,'displayname','prior'); hold on;
            xlim([0,3*theta0(j)]);
        end
        % plot posterior
        [f,xi]=ksdensity(samp(:,j));
        plot(xi,f,'linewidth',1.5,'displayname','posterior'); hold on;
        % add truth
        plot(theta0(j).*ones(1,2),ylim,'linewidth',2,'displayname','truth'); hold on;
        set(gca,'fontsize',14,'box','off','tickdir','out');
        xlabel(marg_name{j},'fontsize',14);
        if j==1
            ylabel('probability density','fontsize',14);
            h_lgd=legend('show','location','best');
            set(h_lgd,'fontsize',14,'box','off');
        end
    end
end
% save plot
fig.PaperPositionMode = 'auto';
print(fig,'./summary/postest','-dpng','-r0');