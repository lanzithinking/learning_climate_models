% batch job

job=batch('Lorenz96_ms');

wait(job);
diary(job,'Lorenz96_ms_diary');
load(job);

delete(job);
clear job;