% Fitting Parameters of Stochastic Dynamical Systems To Equilibrium
% Statistics example

function []=Lorenz96_msi(i)
if ~exist('i','var')
    i=1;
end

% clear;
addpath('../','../../sampler/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2017);
RandStream.setGlobalStream(seed);

% settings
K=36; J=10; dim_sys=(1+J)*K;
ord_momts=1:2; cmp_thld=K;
% parameters for Euler-Maruyama discretization of the ODE
N=1e3; T=N.^(2/3); dt=T./N;

% define ODE
ODE.K=K; ODE.J=J; ODE.dim=dim_sys;
ODE.T=T; ODE.N=N;
ODE.eveq=false; % evaluation at equally spaced points
% simulate data
theta0=[10;1;10;10];
dim_par=length(theta0);
ODE.model=@(t,X)F(X(1:ODE.K),X(1+ODE.K:end),theta0); ODE.init=randn(ODE.dim,1);
y=g(theta0,ODE,ord_momts);
% define data
data.y=y;
data.gamma=sqrt(T/10);
data.ord=ord_momts;
data.cmp=1:cmp_thld;
% define prior
prior.mean=zeros(dim_par,1); prior.mean(1)=5; prior.mean(3)=1;
% prior.cholC=eye(dim_par);
prior.C=eye(dim_par);

% sampling setting
stepsz=[1e-2]; Nleap=[1];
% alg={'pCN'};
alg={'RWM'};
Nsamp=1e3; burnrate=0.1; thinning=2;

% initializatioin
parameter=randn(dim_par,1);
parameter(1)=8+parameter(1);
parameter(3)=4+abs(parameter(3));
% parameter=theta0+.01.*randn(dim_par,1);
% define MCMC
% MCMC=geoinfMC(parameter,prior,@(q,opt)geom(q,prior,data,ODE),alg{1},stepsz,Nleap,[-Inf;-Inf;0;-Inf]);
% MCMC=geoMC(parameter,@(q,opt)geom(q,prior,data,ODE),alg{1},stepsz,Nleap,[-Inf;-Inf;0;-Inf]);
lb=[-Inf;-Inf;0;-Inf]; id=(1:dim_par)';
MCMC=geoMC(parameter(i),@(q,opt)geom(q.*(id==i)+theta0.*(id~=i),prior,data,ODE),alg{1},stepsz,Nleap,lb(i));

% use given MCMC to collect samples
[acpt,time,f_name]=MCMC.sample(Nsamp,burnrate,thinning);

% append additional data/model information
save(['./result/',f_name],'seed','theta0','ODE','data','prior','parameter','-append');
% rename
if exist(['./result/',f_name,'.mat'],'file')==2
    movefile(['./result/',f_name,'.mat'],['./result/theta',num2str(i),'_',f_name,'.mat']);
end

% summarize
fprintf('\nIt takes %.2f seconds to collect %d samples after thinning %d.\n', time,Nsamp,thinning);
fprintf('\nThe final acceptance rate is: %.2f\n\n',acpt);
% efficiency measurement
addpath('./result/');
CalculateStatistics(f_name,'./result/');
% % some plots
% load(['./result/',f_name]);
% fig1=figure(1); set(fig1,'pos',[0 800 900 300]);
% idx=floor(linspace(1,size(samp,1),min([1e4,size(samp,1)])));
% % dim2plot=[1,ceil(dim_par/2),dim_par];
% dim2plot=1:dim_par;
% subplot(1,3,1);
% plot(samp(idx,dim2plot));
% subplot(1,3,2);
% % plotmatrix(samp(idx,dim));
% histfit(samp(idx,dim2plot));
% subplot(1,3,3);
% plot(engy);