% Fitting Parameters of Stochastic Dynamical Systems To Equilibrium
% Statistics example

clear;
addpath('../','../../sampler/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2017);
RandStream.setGlobalStream(seed);

% settings
K=36; J=10; dim_sys=(1+J)*K;
ord_momts=1:2; cmp_thld=K;
% parameters for Euler-Maruyama discretization of the ODE
% N=1e4; T=N.^(2/3); dt=T./N;
T=100; N=floor(T.^(3/2)); dt=T./N;

% define ODE
ODE.K=K; ODE.J=J; ODE.dim=dim_sys;
ODE.T=T; ODE.N=N;
ODE.eveq=true; % evaluation at equally spaced points
ODE.updtinit=true; % update initialization for the next solution
% ODE.thld=1e-2; % threshold for stopping solving ODE
% simulate data
theta0=[10;1;10;10];
dim_par=length(theta0);
ODE.model=@(t,X)F(X(1:ODE.K),X(1+ODE.K:end),theta0); ODE.init=randn(ODE.dim,1);
% y=g(theta0,ODE,ord_momts);
y_fast=g2_fast2_constX(theta0(2:end),ODE,ord_momts);
% define data
data.y=y_fast;
[~,data.var]=get_mvar4phi_fast2_constX(theta0(2:end),ODE,[],ord_momts);
r=0.5; % proportion of standard deviation
data.gamma=sqrt(T).*sqrt(data.var).*r;
data.ord=ord_momts;
data.cmp=1:cmp_thld;
% define prior
prior.mean=[10;0;2;5]; % Normal prior for F,h,b
prior.var=[10;1;.1;10]; % log-Normal prior for c

%% update theta2=(h,c,b)

% adjust ODE
% ODE.N=floor(N/theta0(3)); ODE.T=ODE.N^(2/3);
ODE.T=20; ODE.N=floor(ODE.T.^(3/2));
% separate data/prior for the fast components
% data_fast.y=chos_cmp(data.y,ODE.K,data.ord,data.cmp);
% data_fast.y=chos_cmp2(data.y,ODE.dim,data.ord,1+ODE.K:ODE.dim);
data_fast.y=data.y;
data_fast.gamma=data.gamma.*sqrt(ODE.T/T);
k=1; % slow variable chosen to feed in fast system
data_fast.ord=data.ord; data_fast.cmp=k;
prior_fast.mean=prior.mean(2:end); prior_fast.var=prior.var(2:end); % for h,c,b

% sampling setting
stepsz=[2e-1]; Nleap=[1];
% alg={'pCN'};
alg={'RWM'};
Nsamp=1e3; burnrate=0.1; thinning=2;

% initializatioin
% parameter=randn(dim_par-1,1);
% parameter(1)=abs(parameter(1));
% parameter(2:end)=5+abs(parameter(2:end));
% parameter=[0.9465;10.8225;8.4841]; % from EnKF M10 nz1
parameter=[0.9787;9.4516;9.7211]; % from EnKF M100 nz0.5
% % obtain solutions for slow variables
% sol_slow=ode15s(ODE.model,[0,max([ODE.T,1e1])],ODE.init);
% sol_slow.y=sol_slow.y(1:ODE.K,:);
% sol_slow.idata.dif3d=sol_slow.idata.dif3d(1:ODE.K,:,:);
% define MCMC
% MCMC=geoinfMC(parameter,prior,@(q,opt)geom(q,prior,data_fast,ODE),alg{1},stepsz,Nleap);
% MCMC=geoMC(parameter(1),ODE,@(q,opt,ODE)geom_fast([q;parameter(2:end)],prior_fast,data_fast,ODE,sol_slow),alg{1},stepsz,Nleap);
% MCMC=geoMC(parameter([1,3]),ODE,@(q,opt,ODE)geom_fast([q(1);parameter(2);q(2)],prior_fast,data_fast,ODE,sol_slow),alg{1},stepsz,Nleap);
% MCMC=geoMC(parameter,ODE,@(q,opt,ODE)geom_fast2(q,prior_fast,data_fast,ODE,sol_slow),alg{1},stepsz,Nleap);
MCMC=geoMC(parameter,ODE,@(q,opt,ODE)geom_fast2_constX(q,prior_fast,data_fast,ODE),alg{1},stepsz,Nleap);

% use given MCMC to collect samples
[acpt,time,f_name]=MCMC.sample(Nsamp,burnrate,thinning);

% append additional data/model information
save(['./result/',f_name],'seed','theta0','ODE','data','prior','parameter','-append');
% rename
if exist(['./result/',f_name,'.mat'],'file')==2
    movefile(['./result/',f_name,'.mat'],['./result/ms2_',f_name,'.mat']);
end

% summarize
fprintf('\nIt takes %.2f seconds to collect %d samples after thinning %d.\n', time,Nsamp,thinning);
fprintf('\nThe final acceptance rate is: %.2f\n\n',acpt);
% efficiency measurement
addpath('./result/');
CalculateStatistics(f_name,'./result/');

% %% update full parameter
% 
% % adjust ODE
% ODE.N=N; ODE.T=ODE.N^(2/3);
% % update theta2
% load(['./result/',f_name,'.mat'],'samp');
% prior.mean(2)=mean(log(samp(:,1))); prior.var(2)=var(log(samp(:,1)));
% prior.mean(3:end)=mean(samp(:,2:end)); prior.var(2)=var(samp(:,2:end));
% 
% % sampling setting
% stepsz=[1e-1]; Nleap=[1];
% % alg={'pCN'};
% alg={'RWM'};
% Nsamp=1e3; burnrate=0.1; thinning=2;
% 
% % initializatioin
% parameter=[8+abs(randn),prior.mean(2:end)];
% % parameter=[10.0326;0.9465;10.8225;8.4841]; % from EnKF nz1
% % define MCMC
% % MCMC=geoinfMC(parameter,prior,@(q,opt)geom(q,prior,data,ODE),alg{1},stepsz,Nleap,[-Inf;-Inf;0;-Inf]);
% MCMC=geoMC(parameter,ODE,@(q,opt,ODE)geom(q,prior,data,ODE),alg{1},stepsz,Nleap,[-Inf;-Inf;0;0]);
% 
% % use given MCMC to collect samples
% [acpt,time,f_name]=MCMC.sample(Nsamp,burnrate,thinning);
% 
% % append additional data/model information
% save(['./result/',f_name],'seed','theta0','ODE','data','prior','parameter','-append');
% % rename
% if exist(['./result/',f_name,'.mat'],'file')==2
%     movefile(['./result/',f_name,'.mat'],['./result/ms2_',f_name,'.mat']);
% end
% 
% % summarize
% fprintf('\nIt takes %.2f seconds to collect %d samples after thinning %d.\n', time,Nsamp,thinning);
% fprintf('\nThe final acceptance rate is: %.2f\n\n',acpt);
% % efficiency measurement
% addpath('./result/');
% CalculateStatistics(f_name,'./result/');
% 
% %% some plots
% % load(['./result/',f_name]);
% % fig1=figure(1); set(fig1,'pos',[0 800 900 300]);
% % idx=floor(linspace(1,size(samp,1),min([1e4,size(samp,1)])));
% % % dim2plot=[1,ceil(dim_par/2),dim_par];
% % dim2plot=1:dim_par;
% % subplot(1,3,1);
% % plot(samp(idx,dim2plot));
% % subplot(1,3,2);
% % % plotmatrix(samp(idx,dim));
% % histfit(samp(idx,dim2plot));
% % subplot(1,3,3);
% % plot(engy);