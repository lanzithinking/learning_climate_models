% no analytic moments in non-linear case
% use the result of a long simulation instead

function truth=g2(parameter,ODE,ord)
if ~exist('parameter','var')
    parameter=[10;1;10;10];
end
if ~exist('ODE','var')
    ODE.K=36; ODE.J=10; ODE.dim=(1+ODE.J)*ODE.K;
    ODE.model=@(t,X)F(X(1:ODE.K),X(1+ODE.K:end),parameter);ODE.init=randn(ODE.dim,1);
    ODE.N=1e5; ODE.T=ODE.N^(2/3);
end
if ~exist('ord','var')
    ord=2;
end
% update ODE
ODE.model=@(t,X)F(X(1:ODE.K),X(1+ODE.K:end),parameter);

f_name=['./result/truth_K',num2str(ODE.K),'_J',num2str(ODE.J),'_v2.mat'];
if exist(f_name,'file')==2 && isequal(parameter,[10;1;10;10])
    load(f_name,'truth');
    if all(ord==1)
        truth=truth(1:ODE.dim);
    end
else
    seed = RandStream('mt19937ar','Seed',2017);
    RandStream.setGlobalStream(seed);
    % treat the approximation result based on long time solution as truth
    addpath('./v2/');
    truth=g_T(parameter,ODE,ord);
    save(f_name,'truth');
end

end