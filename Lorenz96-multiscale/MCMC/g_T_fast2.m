function est_momts=g_T_fast2(parameter,ODE,slow,ord,cmp)
if ~exist('parameter','var')
    parameter=[1;10;10];
end
if ~exist('ODE','var')
    ODE.K=36; ODE.J=10; ODE.dim=(1+ODE.J)*ODE.K;
    ODE.init=randn(ODE.dim,1); ODE.model=@(t,X)F_fast(X,ODE.init(1:ODE.K),parameter);
    ODE.N=1e4; ODE.T=ODE.N^(2/3);
    ODE.eveq=false; % evaluation at equally spaced points
end
if ~exist('slow','var') || isempty(slow)
    slow=@(t)ODE.init(1:ODE.K);
end
if ~exist('ord','var')
    ord=1:2;
end
if ~exist('cmp','var') || length(cmp) > ODE.J*ODE.K
    cmp=1:ODE.K;
end
% components' position in the whole vector
pos=(1:ODE.J)'+(cmp-1)*ODE.J;
% update ODE
ODE.model=@(t,X)F_fast(X,slow(t),parameter);
% solve ODE
% [t,x_t]=ode15s(ODE.model,[0,ODE.T],ODE.init); % too large to store
T_cur=0;T_intvl=1e1;evpt_intvl=0:ODE.T/ODE.N:T_intvl;
sol=ode15s(ODE.model,[T_cur,T_cur+T_intvl],ODE.init(1+ODE.K:end));
if ODE.eveq
    ev_x=T_cur+evpt_intvl;
    if T_cur+T_intvl>ODE.T
        ev_x=ev_x(ev_x<=ODE.T);
    end
    ev_y=deval(sol,ev_x,pos);
else
    ev_x=sol.x;
    ev_y=sol.y(pos,:);
end
% get empirical moments
est_momts=update_momt([],ev_x,phi2(ev_y,ord),ODE.eveq);
T_cur=T_cur+T_intvl;
% plot the solution
if 0
    fig1=figure(1); clf(fig1);
    set(fig1,'pos',[0 800 1000 300]);
    plot(sol.x(1:1e3),sol.y(cmp,1:1e3)');
end

% solve ODE sequentially
while T_cur<ODE.T
    N_intvl=length(sol.x);
    sol=odextend(sol,[],min([T_cur+T_intvl,ODE.T]));
    % delete previous history to release space
    sol.x(1:N_intvl-1)=[]; sol.y(:,1:N_intvl-1)=[];
    sol.idata.kvec(1:N_intvl-1)=[]; sol.idata.dif3d(:,:,1:N_intvl-1)=[];
    if ODE.eveq
        ev_x=T_cur+evpt_intvl;
        if T_cur+T_intvl>ODE.T
            ev_x=ev_x(ev_x<=ODE.T);
        end
        ev_y=deval(sol,ev_x,pos);
    else
        ev_x=sol.x;
        ev_y=sol.y(pos,:);
    end
    est_momts_=est_momts;
    % update empirical estimates of moments
    est_momts=update_momt(est_momts,ev_x,phi2(ev_y,ord),ODE.eveq);
    T_cur=T_cur+T_intvl;
end

% d=length(pos);
% % number of co-moments of order at most ord: m=nchoosek(d+ord,ord)-1,
% % excluding the 0-moment
% max_ord=max(ord);
% est=zeros(nchoosek(d+max_ord,max_ord)-1,1); % m x 1
% 
% % calculate the co-moments (mixed) of order at most 2
% if any(ord==1)
%     est(1:nchoosek(d+1-1,1))=m_est;
% end
% if any(ord==2)
%     est(nchoosek(d+1,1)-1+(1:nchoosek(d+2-1,2)))=vech(cov_est); % vech: lower triangular portion operator
% end
% if any(ord>2)
%     warning('Co-moments of order more than 2 are not available yet!');
% end

% print summary
if exist('est_momts_','var')
    fprintf('Relative error in estimated moments: %.8f\n',norm(est_momts-est_momts_)./norm(est_momts_));
end

end