% retrieve the data of moments for chosen position components from that for
% for all positions of dimension dim.


function y_cmp=chos_cmp(y,dim,ord,cmp,sys)
if ~exist('ord','var')
    ord=1:2;
end
if ~exist('cmp','var') || max(cmp) > dim
    cmp=1:dim;
end
if ~exist('sys','var')
    sys='both';
end
if size(y,1)==1
    y=y';
end

y_ord1=[];y_ord2=[];
switch sys
    case 'both'
        fs_sys{1}=':'; fs_sys{2}=fs_sys{1};
    case 'slow'
        fs_sys{1}=1; fs_sys{2}=fs_sys{1};%+(0:1);
    case 'fast'
        fs_sys{1}=2; fs_sys{2}=fs_sys{1}+1;%+(0:1);
    otherwise
        error('Wrong specification on system components!');
end

if any(ord==1)
    y_ord1=reshape(y(1:2*dim),dim,[]);
    y_ord1=y_ord1(cmp,fs_sys{1});
    y_ord1=y_ord1(:);
end
if any(ord==2)
    y_ord2=reshape(y(1+2*dim:end),dim,[]);
    y_ord2=y_ord2(cmp,fs_sys{2});
    y_ord2=y_ord2(:);
end
if any(ord>2)
    warning('Co-moments of order more than 2 are not available yet!');
end

y_cmp=[y_ord1;y_ord2];

end
