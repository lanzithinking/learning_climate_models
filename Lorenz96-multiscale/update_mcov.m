% sequentially update empirical mean and covariance

function [cumN_updated,mean_updated,cov_updated]=update_mcov(cumN_current,mean_current,cov_current,x_newbatch,ord)
mean_updated=[]; cov_updated=[];
[dim,batch_sz]=size(x_newbatch);
if cumN_current==0 || isempty(mean_current)
    mean_current=zeros(dim,1);
end
if cumN_current==0 || isempty(cov_current)
    cov_current=zeros(dim);
end
if ~exist('ord','var')
    ord=1;
end

% update cumulative sample size N
cumN_updated=cumN_current+batch_sz;

% update empirical mean
if any(ord==1)
    x_mean=mean(x_newbatch,2);
    mean_updated=(cumN_current.*mean_current+batch_sz.*x_mean)./cumN_updated; % (D,1)
end

% rank-2 update empirical covariance
if any(ord==2)
    rk2update=cumN_current*batch_sz/cumN_updated.*[mean_current,x_mean]*[1,-1;-1,1]*[mean_current';x_mean'];
    cov_updated=((cumN_current-1).*cov_current+(batch_sz-1).*cov(x_newbatch')+rk2update)./(cumN_updated-1); % (D,D)
end
