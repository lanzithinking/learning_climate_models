% sequentially update empirical moments

function momts_updated=update_momt(momts_current,x_newbatch,phi_newbatch,eveq,spinup)
if isempty(momts_current)
    momts_current=0;
end
if ~exist('eveq','var')
    eveq=false;
end
if ~exist('spinup','var')
    spinup=0;
end


if x_newbatch(end)>spinup
    if x_newbatch(1)<spinup
        idx=(x_newbatch>spinup);
        x_newbatch=x_newbatch(idx); phi_newbatch=phi_newbatch(:,idx);
    end
    if eveq
        int_incr=sum(phi_newbatch,2).*range(x_newbatch)./(length(x_newbatch)-1);
    else
        int_incr=trapz(x_newbatch,phi_newbatch,2);
    end
    momts_updated=((x_newbatch(1)-spinup).*momts_current+int_incr)./(x_newbatch(end)-spinup); % (1,m)
else
    momts_updated=momts_current;
end

