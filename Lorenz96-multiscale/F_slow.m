function f=F_slow(x,m_fast,parameter)
if ~exist('parameter','var')
    parameter=[10;10]; % F,hc
end
K=size(x,1);

f=-x(circshift(1:K,1),:).*(x(circshift(1:K,2),:) - x(circshift(1:K,-1),:)) - x + parameter(1) - parameter(2).*m_fast;

end

% same effect as circshift(idx,offset) %
% function new_idx=shiftidx(idx,offset)
% L=length(idx);
% new_idx=idx(mod(-offset-1+(1:L),L)+1);
% end