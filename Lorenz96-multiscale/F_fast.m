function f=F_fast(y,slow,parameter)
if ~exist('parameter','var')
    parameter=[1;10;10]; % h,c,b
end
JK=size(y,1); K=length(slow); J=JK/K;
slow=kron(slow,ones(J,1));

f=(-parameter(3).*y(circshift(1:JK,-1),:).*(y(circshift(1:JK,-2),:) - y(circshift(1:JK,1),:)) - y + parameter(1)/J.*slow).*parameter(2);

end

% same effect as circshift(idx,offset) %
% function new_idx=shiftidx(idx,offset)
% L=length(idx);
% new_idx=idx(mod(-offset-1+(1:L),L)+1);
% end