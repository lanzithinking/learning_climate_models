function [est_momts,ODE,poolobj]=g_T_par(parameter,ODE,ord,cmp,poolobj)
if ~exist('parameter','var')
    parameter=[10;1;10;10];
end
if ~exist('ODE','var')
    ODE.K=36; ODE.J=10; ODE.dim=(1+ODE.J)*ODE.K;
    ODE.model=@(t,X)F(X(1:ODE.K),X(1+ODE.K:end),parameter);ODE.init=randn(ODE.dim,1);
    ODE.N=1e4; ODE.T=ODE.N^(2/3);
    ODE.eveq=false; % evaluation at equally spaced points
end
if ~exist('ord','var')
    ord=1:2;
end
if ~exist('cmp','var') || max(cmp) > ODE.K
    cmp=1:ODE.K;
end
% number of parallel jobs
N_par=size(parameter,2);
% setup parallel enviroment
if ~exist('poolobj','var') || isempty(poolobj)
    poolobj=parpool('local',min([20,N_par]));
end

est_momts=cell(1,N_par);
parfor j=1:N_par
    est_momts{j}=g_T(parameter(:,j),ODE,ord,cmp);
end
est_momts=cell2mat(est_momts);

% update initial value for the next solution
if isfield(ODE,'updtinit') && ODE.updtinit
    updtX=ODE.init(1:ODE.K); updtY=reshape(ODE.init(ODE.K+1:end),ODE.J,ODE.K);
    l_cmp=length(cmp);
    updtX(cmp)=mean(est_momts(1:l_cmp,:),2);
    updtY(:,cmp)=repmat(mean(est_momts(l_cmp+(1:l_cmp),:),2)',ODE.J,1);
    ODE.init=[updtX;updtY(:)];
end

end