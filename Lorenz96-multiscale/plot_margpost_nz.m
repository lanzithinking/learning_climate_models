% plot marginal posteriors with different noise levels

clear;
addpath('../tight_subplot/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2017);
RandStream.setGlobalStream(seed);

% parameters for setting
nzlvl=[0.1,0.2,0.5,1.0];
L_nzlvl=length(nzlvl);
N=3e3;
% N=1e6;
theta0=[10;1;10;10];
dim_par=length(theta0);

% plot setting
% fig=figure('visible','off'); %clf;
fig=figure(1); clf(fig);
% set(fig,'pos',[0 800 1000 350]);
width=7.25; height=width/1.63;
init_figure(height,width);
ha=tight_subplot(1,dim_par,[0,.09],[.14,.1],[.05,.03]);
lty={'-','--','-.',':'};
clr={'blue','red'};

% setup the plotting parameters
% L=151;
% rangs=zeros(L,dim_par);
% for j=1:dim_par
%     rangs(:,j)=linspace(-theta0(j),2*theta0(j),L);
% end
% rangs(:,3)=theta0(3)+rangs(:,3);
% margposts=zeros(L,dim_par);
marg_name={'F','h','c','b'};

% load data
files = dir('./result');
nfiles = length(files) - 2;
found=false;
for k=1:nfiles
    if ~isempty(strfind(files(k+2).name,['_N',num2str(N,1),'_nz1.mat']))
        load(strcat('./result/', files(k+2).name));
        fprintf('%s loaded.\n',files(k+2).name);
        found=true; break;
    end
end
% plot marginal posteriors
if found
    % define the log-prior
    logprior=@(parameter).5*sum(([parameter(:,1:2),log(abs(parameter(:,3))),parameter(:,4)]-prior.mean').^2./prior.var',2) + log(abs(parameter(:,3)));
    for j=1:dim_par
        subplot(ha(j));
        margpri_j=logprior(theta0'.*(1:dim_par~=j)+rangs.*(1:dim_par==j));
        marglik_j=margposts(:,j)-margpri_j;
        for i=1:L_nzlvl
%             semilogy(rangs(:,j),marglik_j./nzlvl(i).^2+margpri_j,'linestyle',lty{i},'linewidth',1.2,'displayname',['r=',num2str(nzlvl(i),'%.1f')]); hold on;
            semilogy(rangs(:,j),marglik_j./nzlvl(i).^2+margpri_j,'linewidth',1.2,'displayname',['r=',num2str(nzlvl(i),'%.1f')]); hold on;
            xlim([min(rangs(:,j)),max(rangs(:,j))]);
%             ylim([1,1e4]);
        end
        set(gca,'fontsize',14,'box','off','yminortick','off','tickdir','out');
        xlabel(marg_name{j},'fontsize',14);
        if j==1
            ylabel('potential energy','fontsize',14);
            h_lgd=legend('show','location','best');
            set(h_lgd,'fontsize',14,'box','off');
        end
    end
end

fig.PaperPositionMode = 'auto';
print(fig,['./result/margpost_nz_N',num2str(N,1)],'-dpng','-r0');
