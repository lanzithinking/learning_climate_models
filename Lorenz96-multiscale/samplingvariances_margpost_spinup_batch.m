% batch job

job=batch('samplingvariances_margpost_spinup','Pool',10);

wait(job);
diary(job,'samplingvariances_margpost_spinup_diary');
load(job);

delete(job);
clear job;