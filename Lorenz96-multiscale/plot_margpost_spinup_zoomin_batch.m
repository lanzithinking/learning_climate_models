% batch job

job=batch('plot_margpost_spinup_zoomin','Pool',20);

wait(job);
diary(job,'plot_margpost_spinup_zoomin_diary');
load(job);

delete(job);
clear job;