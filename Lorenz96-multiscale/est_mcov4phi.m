% estimate mean and covariance of moments phi

clear;
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2017);
RandStream.setGlobalStream(seed);

% settings
K=36; J=10; dim_sys=(1+J)*K;
ord_momts=1:2; cmp_thld=K;
% parameters for Euler-Maruyama discretization of the ODE
N=1e7; T=N.^(2/3); dt=T./N;

% define ODE
ODE.K=K; ODE.J=J; ODE.dim=dim_sys;
ODE.T=T; ODE.N=N;
ODE.eveq=true; % evaluation at equally spaced points
% simulate data
theta0=[10;1;10;10];
dim_par=length(theta0);
ODE.model=@(t,X)F(X(1:ODE.K),X(1+ODE.K:end),theta0); ODE.init=randn(ODE.dim,1);
parameter=theta0;

% setting for solution
ord=ord_momts;cmp=1:cmp_thld;
% number of components
k=length(cmp);
% components' position in the whole vector
pos_f=ODE.K+((1:ODE.J)'+(cmp-1)*ODE.J);
pos=[cmp(:);pos_f(:)];
% solve SDE
T_cur=0;T_intvl=1e2;evpt_intvl=0:ODE.T/ODE.N:T_intvl;
sol=ode15s(ODE.model,[T_cur,T_cur+T_intvl],ODE.init);
if ODE.eveq
    ev_x=T_cur+evpt_intvl;
    if T_cur+T_intvl>ODE.T
        ev_x=ev_x(ev_x<=ODE.T);
    end
    ev_y=deval(sol,ev_x,pos);
else
    ev_x=sol.x;
    ev_y=sol.y(pos,:);
end
% initialization
cumN=0;
% get empirical mean and covariance of phi
[cumN,m_est,cov_est]=update_mcov(cumN,[],[],phi(ev_y,k,ODE.J,ord),ord);
T_cur=T_cur+T_intvl;

% record results
relerr=[]; h_est=[];

% solve ODE sequentially
tic;
while T_cur<ODE.T
    N_intvl=length(sol.x);
    sol=odextend(sol,[],min([T_cur+T_intvl,ODE.T]));
    % delete previous history to release space
    sol.x(1:N_intvl-1)=[]; sol.y(:,1:N_intvl-1)=[];
    sol.idata.kvec(1:N_intvl-1)=[]; sol.idata.dif3d(:,:,1:N_intvl-1)=[];
    if ODE.eveq
        ev_x=T_cur+evpt_intvl;
        if T_cur+T_intvl>ODE.T
            ev_x=ev_x(ev_x<=ODE.T);
        end
        ev_y=deval(sol,ev_x,pos);
    else
        ev_x=sol.x;
        ev_y=sol.y(pos,:);
    end
    m_est_=m_est;
    % update empirical estimates of mean and covariance
    [cumN,m_est,cov_est]=update_mcov(cumN,m_est,cov_est,phi(ev_y,k,ODE.J,ord),ord);
    T_cur=T_cur+T_intvl;
    % calculate relative error and update plot
    relerr_cur=norm(m_est-m_est_)./norm(m_est_);
    fprintf('Relative error in estimated moments: %.8f\n',relerr_cur);
    relerr=[relerr;relerr_cur];
    % estimate h = <Y2_bar>/<XY_bar>*J
    if any(ord==2)
        XY_bar_avg=mean(m_est(end-2*k+1:end-k));
        Y2_bar_avg=mean(m_est(end-k+1:end));
        h_est_cur=Y2_bar_avg/XY_bar_avg*ODE.J;
        fprintf('Estimated h: %.4f; differing by %.4f from the truth.\n',h_est_cur,h_est_cur-parameter(2));
        h_est=[h_est;h_est_cur];
    end
end
time=toc;
fprintf('It took %.2f seconds to get the estimates.\n',time);

% save the results
f_name=['./result/truth_mcov_K',num2str(ODE.K),'_J',num2str(ODE.J),'.mat'];
save(f_name,'ODE','parameter','m_est','cov_est','relerr','h_est');