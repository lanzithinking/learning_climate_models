function est_momts=g_T(parameter,ODE,ord,cmp)
if ~exist('parameter','var')
    parameter=[10;1;10;10];
end
if ~exist('ODE','var')
    ODE.K=36; ODE.J=10; ODE.dim=(1+ODE.J)*ODE.K;
    ODE.model=@(t,X)F(X(1:ODE.K),X(1+ODE.K:end),parameter);ODE.init=randn(ODE.dim,1);
    ODE.N=1e4; ODE.T=ODE.N^(2/3);
    ODE.eveq=false; % evaluation at equally spaced points
    ODE.spinup=0; % spin up (burn in) some initial solution points
end
if ~exist('ord','var')
    ord=1:2;
end
if ~exist('cmp','var') || max(cmp) > ODE.K
    cmp=1:ODE.K;
end
if ~isfield(ODE,'spinup')
    ODE.spinup=0;
end
% number of components
k=length(cmp);
% components' position in the whole vector
pos_f=ODE.K+((1:ODE.J)'+(cmp-1)*ODE.J);
pos=[cmp(:);pos_f(:)];
% update ODE
ODE.model=@(t,X)F(X(1:ODE.K),X(1+ODE.K:end),parameter);
% solve ODE
% [t,x_t]=ode15s(ODE.model,[0,ODE.T],ODE.init); % too large to store
T_cur=0;T_intvl=1e2;evpt_intvl=0:ODE.T/ODE.N:T_intvl;
sol=ode15s(ODE.model,[T_cur,T_cur+T_intvl],ODE.init);
if ODE.eveq
    ev_x=T_cur+evpt_intvl;
    if T_cur+T_intvl>ODE.T
        ev_x=ev_x(ev_x<=ODE.T);
    end
    ev_y=deval(sol,ev_x,pos);
else
    ev_x=sol.x;
    ev_y=sol.y(pos,:);
end
% get empirical moments
est_momts=update_momt([],ev_x,phi(ev_y,k,ODE.J,ord),ODE.eveq,ODE.spinup);
T_cur=T_cur+T_intvl;
% plot the solution
if 0
    fig1=figure(1); clf(fig1);
    set(fig1,'pos',[0 800 1000 300]);
    plot(sol.x(1:1e3),sol.y(pos,1:1e3));
end

% solve ODE sequentially
while T_cur<ODE.T
    N_intvl=length(sol.x);
    sol=odextend(sol,[],min([T_cur+T_intvl,ODE.T]));
    % delete previous history to release space
    sol.x(1:N_intvl-1)=[]; sol.y(:,1:N_intvl-1)=[];
    sol.idata.kvec(1:N_intvl-1)=[]; sol.idata.dif3d(:,:,1:N_intvl-1)=[];
    if ODE.eveq
        ev_x=T_cur+evpt_intvl;
        if T_cur+T_intvl>ODE.T
            ev_x=ev_x(ev_x<=ODE.T);
        end
        ev_y=deval(sol,ev_x,pos);
    else
        ev_x=sol.x;
        ev_y=sol.y(pos,:);
    end
    est_momts_=est_momts;
    % update empirical estimates of moments
    est_momts=update_momt(est_momts,ev_x,phi(ev_y,k,ODE.J,ord),ODE.eveq,ODE.spinup);
    T_cur=T_cur+T_intvl;
    % estimate h = <Y2_bar>/<XY_bar>*J
%     if any(ord==2) && isfield(ODE,'thld')
%         XY_bar_avg=mean(est_momts(end-2*k+1:end-k));
%         Y2_bar_avg=mean(est_momts(end-k+1:end));
%         h_est=Y2_bar_avg/XY_bar_avg*ODE.J;
%         if abs(h_est-parameter(2))<=ODE.thld
%             fprintf('Break at time %.2f: no need for solving longer!\n',T_cur);
%             break
%         end
%     end
    if isfield(ODE,'thld')
        rerr=norm(est_momts-est_momts_)/norm(est_momts_);
        if rerr<=ODE.thld
            fprintf('Break at time %.2f: no need for solving longer!\n',T_cur);
            break
        end
    end
end

% print summary
if exist('est_momts_','var')
    fprintf('Relative error in estimated moments: %.8f\n',norm(est_momts-est_momts_)./norm(est_momts_));
end
% estimate h = <Y2_bar>/<XY_bar>*J
if any(ord==2)
    if ~exist('h_est','var')
        XY_bar_avg=mean(est_momts(end-2*k+1:end-k));
        Y2_bar_avg=mean(est_momts(end-k+1:end));
        h_est=Y2_bar_avg/XY_bar_avg*ODE.J;
    end
    fprintf('Estimated h: %.4f; differing by %.4f from the truth.\n',h_est,h_est-parameter(2));
end

end