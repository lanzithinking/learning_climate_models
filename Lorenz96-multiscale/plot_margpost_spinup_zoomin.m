% plot marginal posterior after spinning up (burning in) some initial
% solutions of the dynamics

clear;
addpath('../tight_subplot/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2017);
RandStream.setGlobalStream(seed);

% settings
K=36; J=10; dim_sys=(1+J)*K;
ord_momts=1:2; cmp_thld=K;
% parameters for Euler-Maruyama discretization of the ODE
N=1e6; T=N.^(2/3); dt=T./N;
% T=500; N=floor(T.^(3/2)); dt=T./N;

% define ODE
ODE.K=K; ODE.J=J; ODE.dim=dim_sys;
ODE.T=T; ODE.N=N;
ODE.eveq=true; % evaluation at equally spaced points
ODE.updtinit=false; % update initialization for the next solution
% ODE.thld=1e-3; % threshold for stopping solving ODE
ODE.spinup=100; % spin up (burn in) some initial solution points
% simulate data
theta0=[10;1;10;10];
dim_par=length(theta0);
ODE.model=@(t,X)F(X(1:ODE.K),X(1+ODE.K:end),theta0); ODE.init=randn(ODE.dim,1);
y=g(theta0,ODE,ord_momts);
% define data
data.y=y;
[~,data.var]=get_mvar4phi(theta0,ODE,ord_momts);
r=1; % proportion of standard deviation
data.gamma=sqrt(T).*sqrt(data.var).*r;
data.ord=ord_momts;
data.cmp=1:cmp_thld;
% define prior
prior.mean=[10;0;2;5]; % Normal prior for F,h,b
prior.var=[10;1;.1;10]; % log-Normal prior for c

% define the marginal posterior
margpost=@(theta,marg_dim)geom(theta0.*(1:dim_par~=marg_dim)'+theta.*(1:dim_par==marg_dim)',prior,data,ODE);
marg_name={'F','h','c','b'};

% zoom-in range
zoom_range=[0,10;1,2;10,20;0,10];
% setup the plotting parameters
L=101;
rangs=zeros(L,dim_par);
for j=1:dim_par
    rangs(:,j)=linspace(zoom_range(j,1),zoom_range(j,2),L);
end
margposts=zeros(L,dim_par);

% setup parallel enviroment
% myCluster = parcluster('local');
% myCluster.NumWorkers = 30;  % 'Modified' property now TRUE
% saveProfile(myCluster);     % 'local' profile now updated,
%                             % 'Modified' property now FALSE
if isempty(gcp('nocreate'))
    parpool('local',20);
end
% calculate the marginal posteriors
% for j=1:dim_par
%     tic;
%     parfor i=1:L % inner parfor : not recommended
%         margposts(i,j)=margpost(rangs(i,j),j);
%     end
%     time=toc;
%     fprintf('It took %.2f seconds to calculate the posterior of %s \n',time,marg_name{j});
% end
tic;
parfor i=1:L
    for j=1:dim_par
        margposts(i,j)=margpost(rangs(i,j),j);
    end
end
time=toc;
fprintf('It took %.2f seconds to calculate the posteriors.\n',time);
% save the result
save(['./result/margpost','_ord',num2str(max(ord_momts)),'_N',num2str(N,1),'_nz',num2str(r),'.mat'],'seed','theta0','ODE','data','prior','rangs','margposts');

fig=figure('visible','off'); %clf;
set(fig,'pos',[0 800 1200 350]);
ha=tight_subplot(1,dim_par,[0,.04],[.14,.1],[.05,.03]);
for j=1:dim_par
    subplot(ha(j));
%     plot(rangs(:,j),margposts(:,j));
    semilogy(rangs(:,j),margposts(:,j));
    set(gca,'fontsize',14);
    xlabel(marg_name{j},'fontsize',14);
    if j==1
        ylabel('potential energy','fontsize',14);
    end
end
fig.PaperPositionMode = 'auto';
print(fig,['./result/margpost','_ord',num2str(max(ord_momts)),'_N',num2str(N,1),'_nz',num2str(r)],'-dpng','-r0');

% delete the pool
delete(gcp('nocreate'));