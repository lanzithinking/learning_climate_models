% Test the approximation of g_T

clear;
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2017);
RandStream.setGlobalStream(seed);

% settings
K=36; J=10; dim_sys=(1+J)*K;
ord_momts=1:2; % cmp_threshold=dim_sys;
% parameters for Euler-Maruyama discretization of the ODE
N=1e7; T=N.^(2/3); dt=T./N;

% define ODE
ODE.K=K; ODE.J=J; ODE.dim=dim_sys;
ODE.T=T; ODE.N=N;
ODE.eveq=false; % evaluation at equally spaced points
% simulate data
theta=[10;1;10;10];
ODE.model=@(t,X)F(X(1:ODE.K),X(1+ODE.K:end),theta);ODE.init=randn(ODE.dim,1);
tru=g(theta,ODE,ord_momts);
% parameters for Euler-Maruyama discretization of the SDE
set_N=[1e2,1e3,1e4,1e5,1e6];
L_set=length(set_N);
logerr=zeros(L_set,1);logrelerr=zeros(L_set,1);logdt=zeros(L_set,1);
% run multiple comparison
for n=1:L_set

N=set_N(n); T=N.^(2/3); dt=T./N;
% define sde
ODE.T=T; ODE.N=N;
ODE.init=randn(ODE.dim,1);

% estimate moments
est=g_T(theta,ODE,ord_momts);
% errors
err=norm(est-tru);
relerr=err/norm(tru);
fprintf('With %.0e discretization points, the difference between g_T(theta) and g(theta) is %.4f\n',N,err);
fprintf('With %.0e discretization points, the relative difference between g_T(theta) and g(theta) is %.4f\n',N,relerr);
% record error
logerr(n)=log(err);logrelerr(n)=log(relerr);logdt(n)=log(dt);

end

% save results
save('./result/test_g_T.mat','set_N','logerr','logrelerr','logdt');

% estimate the converge rate by linear regression logerr against logdt
X=[ones(L_set,1),logdt];
% b=X\logerr;
b=X\logrelerr;
fprintf('Estimated convergence rate: %.4f\n',b(2));
% plot the convergence rate
y_fitted=X*b;
% scatter(logdt,logerr);
scatter(logdt,logrelerr);
hold on; grid on;
plot(logdt,y_fitted);
xlabel('Log of discretization size');
% ylabel('Log of error');
ylabel('Log of relative error');
title('Estimated convergence rate');
legend('Data','Linear Regression','Location','best');
hold off;