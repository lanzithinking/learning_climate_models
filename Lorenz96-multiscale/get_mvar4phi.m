% get mean and covariance of moments phi

function [m_phi,var_phi]=get_mvar4phi(parameter,ODE,ord)
if ~exist('parameter','var')
    parameter=[10;1;10;10];
end
if ~exist('ODE','var')
    ODE.K=36; ODE.J=10; ODE.dim=(1+ODE.J)*ODE.K;
    ODE.model=@(t,X)F(X(1:ODE.K),X(1+ODE.K:end),parameter);ODE.init=randn(ODE.dim,1);
    ODE.N=1e7; ODE.T=ODE.N^(2/3);
    ODE.eveq=true; % evaluation at equally spaced points
end
if ~exist('ord','var')
    ord=1:2;
end
% update ODE
ODE.model=@(t,X)F(X(1:ODE.K),X(1+ODE.K:end),parameter);

f_name=['~/PROJ-GPS/WeatherForecast/code/Lorenz96-multiscale/truth_mcov_K',num2str(ODE.K),'_J',num2str(ODE.J),'.mat'];
if exist(f_name,'file')==2 && isequal(parameter,[10;1;10;10])
    load(f_name,'m_est','cov_est');
else
    seed = RandStream('mt19937ar','Seed',2017);
    RandStream.setGlobalStream(seed);
    % treat the approximation result based on long time solution as truth
    % solve SDE
    T_cur=0;T_intvl=1e2;evpt_intvl=0:ODE.T/ODE.N:T_intvl;
    sol=ode15s(ODE.model,[T_cur,T_cur+T_intvl],ODE.init);
    if ODE.eveq
        ev_x=T_cur+evpt_intvl;
        if T_cur+T_intvl>ODE.T
            ev_x=ev_x(ev_x<=ODE.T);
        end
        ev_y=deval(sol,ev_x);
    else
        ev_x=sol.x;
        ev_y=sol.y;
    end
    % initialization
    cumN=0;
    % get empirical mean and covariance of phi
    [cumN,m_est,cov_est]=update_mcov(cumN,[],[],phi(ev_y,ODE.K,ODE.J,ord),ord);
    T_cur=T_cur+T_intvl;

    % record results
    relerr=[]; h_est=[];

    % solve ODE sequentially
    tic;
    while T_cur<ODE.T
        N_intvl=length(sol.x);
        sol=odextend(sol,[],min([T_cur+T_intvl,ODE.T]));
        % delete previous history to release space
        sol.x(1:N_intvl-1)=[]; sol.y(:,1:N_intvl-1)=[];
        sol.idata.kvec(1:N_intvl-1)=[]; sol.idata.dif3d(:,:,1:N_intvl-1)=[];
        if ODE.eveq
            ev_x=T_cur+evpt_intvl;
            if T_cur+T_intvl>ODE.T
                ev_x=ev_x(ev_x<=ODE.T);
            end
            ev_y=deval(sol,ev_x);
        else
            ev_x=sol.x;
            ev_y=sol.y;
        end
        m_est_=m_est;
        % update empirical estimates of mean and covariance
        [cumN,m_est,cov_est]=update_mcov(cumN,m_est,cov_est,phi(ev_y,ODE.K,ODE.J,ord),ord);
        T_cur=T_cur+T_intvl;
        % calculate relative error and update plot
        relerr_cur=norm(m_est-m_est_)./norm(m_est_);
        fprintf('Relative error in estimated moments: %.8f\n',relerr_cur);
        relerr=[relerr;relerr_cur];
        % estimate h = <Y2_bar>/<XY_bar>*J
        if any(ord==2)
            XY_bar_avg=mean(m_est(end-2*ODE.K+1:end-ODE.K));
            Y2_bar_avg=mean(m_est(end-ODE.K+1:end));
            h_est_cur=Y2_bar_avg/XY_bar_avg*ODE.J;
            fprintf('Estimated h: %.4f; differing by %.4f from the truth.\n',h_est_cur,h_est_cur-parameter(2));
            h_est=[h_est;h_est_cur];
        end
    end
    time=toc;
    fprintf('It took %.2f seconds to get the estimates.\n',time);
    % save the results
    save(f_name,'ODE','parameter','m_est','cov_est','relerr','h_est');
end
% set values
m_phi=m_est;
var_phi=diag(cov_est);
if all(ord==1)
    m_phi=m_phi(1:2*ODE.K);
    var_phi=var_phi(1:2*ODE.K);
end