function f=F(x,y,parameter)
if ~exist('parameter','var')
    parameter=[10;1;10;10]; % F,h,c,b
end

m_fast=shiftdim(mean(reshape(y,[],length(x)),1),1);
f_x=F_slow(x,m_fast,[parameter(1);parameter(2)*parameter(3)]);
f_y=F_fast(y,x,parameter(2:4));
f=[f_x;f_y];

end

% same effect as circshift(idx,offset) %
% function new_idx=shiftidx(idx,offset)
% L=length(idx);
% new_idx=idx(mod(-offset-1+(1:L),L)+1);
% end