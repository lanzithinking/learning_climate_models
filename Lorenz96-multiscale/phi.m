% moment function for Lorenz96-multiscale system (2 layer)
% particularly, extract X, Y_bar (over j), X * Y_bar and Y2_bar

function momts=phi(SOL,K,J,ord)
X=[];Y_bar=[];X2=[];XY_bar=[];Y2_bar=[];
if ~exist('ord','var')
    ord=1:2;
end

%%%% -- A neat trick by John D'Errico -- %%%%
%%%% https://www.mathworks.com/matlabcentral/newsreader/view_thread/118574
ind=(1:size(SOL,1)-K)';
% blksum=sparse(ceil(ind/J),ind,1);
blkmean=sparse(ceil(ind/J),ind,1/J);

if any(ord==1)
    X=SOL(1:K,:);
%     Y_bar=shiftdim(mean(reshape(SOL(K+1:end,:),J,(size(SOL,1)-K)/J,[]),1),1);
%     Y_bar=blksum*SOL(K+1:end,:)./J;
    Y_bar=blkmean*SOL(K+1:end,:);
end
if any(ord==2)
    X2=X.^2;
    XY_bar=X.*Y_bar;
%     Y2_bar=shiftdim(mean(reshape(SOL(K+1:end,:).^2,J,(size(SOL,1)-K)/J,[]),1),1);
%     Y2_bar=blksum*SOL(K+1:end,:).^2./J;
    Y2_bar=blkmean*SOL(K+1:end,:).^2;
end
if any(ord>2)
    warning('Co-moments of order more than 2 are not available yet!');
end

momts=[X;Y_bar;X2;XY_bar;Y2_bar];

end