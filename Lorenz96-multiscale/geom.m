function [u,g,eigs,ODE]=geom(parameter,prior,data,ODE)
u=[]; g=[]; eigs=[];
if ~isfield(data,'cmp') || max(data.cmp) > ODE.K
    data.cmp=1:ODE.K;
end
est=g_T(parameter,ODE,data.ord,data.cmp);
y_cmp=chos_cmp(data.y,ODE.K,data.ord,data.cmp);
gamma_cmp=chos_cmp(data.gamma,ODE.K,data.ord,data.cmp);

% parameters: F,h,c,b
u=ODE.T./2.*sum(((y_cmp-est)./gamma_cmp).^2) + .5*sum(([parameter(1:2);log(abs(parameter(3)));parameter(4)]-prior.mean).^2./prior.var) + log(abs(parameter(3)));

% update initial value for the next solution
if isfield(ODE,'updtinit') && ODE.updtinit
    updtX=ODE.init(1:ODE.K); updtY=reshape(ODE.init(ODE.K+1:end),ODE.J,ODE.K);
    l_cmp=length(data.cmp);
    updtX(data.cmp)=est(1:l_cmp);
    updtY(:,data.cmp)=repmat(est(l_cmp+(1:l_cmp))',ODE.J,1);
    ODE.init=[updtX;updtY(:)];
end

end