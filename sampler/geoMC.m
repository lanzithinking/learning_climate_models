%%%% - Geometric Finite dimensional MCMC samplers - %%%%
%%%%   Shiwei Lan                                   %%%%
%%%%   CMS, CalTech                                 %%%%
%%%%   slan@caltech.edu; lanzithinking@outlook.com  %%%%
%%%%   Copyright @ 2017                             %%%%
%%%% - Geometric Finite dimensional MCMC samplers - %%%%

classdef geoMC
    properties
        q; dim; % the state and its dimension
        ODE; % ODE instance
        u; % the potential energy function of q
        g; % the (adjusted) negative gradient of log-likelihood
        eigs; % the eigen-pairs for low-rank metric approximation
        geom; % the function to calculate required geometric terms
        h=1; L=10; % the step size and the number of leapfrogs
        alg; % the name of the algorithm
        geom_ord; % the order of geometry quantities
        lb=-Inf; ub=Inf; % lower and upper bound of parameter space
        bdy_hdl='reject'; % boundary handling: reject or bounce
    end
    methods
        % constructor
        function obj=geoMC(q,ODE,geom,alg,h,L,lb,ub,bdy_hdl)
            if ~exist('L','var')
                L=1;
            end
            obj.q=q; obj.dim=length(q);
            obj.ODE=ODE;
            obj.alg=alg;
            obj.geom_ord=0;
            if contains(alg,{'MALA','HMC'})
                obj.geom_ord=1;
            end
            if contains(alg,{'RMALA','RHMC'})
                obj.geom_ord=2;
            end
            obj.geom=@(q,ODE)geom(q,obj.geom_ord,ODE);
            [obj.u,obj.g,obj.eigs,obj.ODE]=obj.geom(q,obj.ODE);
            obj.h=h; obj.L=L;
            if ~contains(alg,'HMC')
                obj.L=1;
            end
            if ~exist('lb','var')
                lb=-Inf.*ones(obj.dim,1);
            end
            if ~exist('ub','var')
                ub=Inf.*ones(obj.dim,1);
            end
            obj.lb=lb; obj.ub=ub;
            if exist('bdy_hdl','var')
                obj.bdy_hdl=bdy_hdl;
            end
        end
        
        % resample v
        function v=resample_aux(obj)
            v=randn(obj.dim,1);
        end
        
        % RWM
        function [obj,acpt_ind]=RWM(obj)
            % sample velocity
            v=obj.resample_aux();
            
            % generate proposal according to random walk
            q=obj.q + obj.h.*v;
            
            % handle the constraint
            switch obj.bdy_hdl
                case 'reject'
                    cons_ind=[q<obj.lb, q>obj.ub];
                    if any(cons_ind(:))
                        acpt_ind=false;
                        return;
                    end
                case 'bounce'
                    while 1
                        cons_ind=[q<obj.lb, q>obj.ub];
                        if any(cons_ind(:,1))
                            idx_l=cons_ind(:,1);
                            q(idx_l)=2.*obj.lb(idx_l) - q(idx_l);
                            v(idx_l)=-v(idx_l);
                        elseif any(cons_ind(:,2))
                            idx_u=cons_ind(:,2);
                            q(idx_u)=2.*obj.ub(idx_u) - q(idx_u);
                            v(idx_u)=-v(idx_u);
                        else
                            break;
                        end
                    end
                otherwise
                    error('Boudary handling option not available!');
            end
            
            % update geometry
            [u,~,~,obj.ODE]=obj.geom(q,obj.ODE);
            
            % Accept according to ratio
            logRatio=-u + obj.u;
            
            if (isfinite(logRatio) && (log(rand) < min([0,logRatio])))
                acpt_ind=true;
                obj.q=q; obj.u=u;
            else
                acpt_ind=false;
            end
        end
        
        % MALA
        function [obj,acpt_ind]=MALA(obj)
        end
        
        % HMC
        function [obj,acpt_ind]=HMC(obj)
        end
        
        % RMALA
        function [obj,acpt_ind]=RMALA(obj)
        end
        
        % RHMC
        function [obj,acpt_ind]=RHMC(obj)
        end
        
        % sample function
        function [acpt,time,f_name]=sample(obj,Nsamp,burnrate,thinning)
            if ~exist('Nsamp','var')
                Nsamp=1e4;
            end
            if ~exist('burnrate','var')
                burnrate=.1;
            end
            if ~exist('thinning','var')
                thinning=1;
            end
            % setting
            Niter=Nsamp*thinning;
            NBurnIn=floor(Niter*burnrate);
            Niter=Niter+ NBurnIn;
            % allocation to save
            samp=zeros(Nsamp,obj.dim);
            engy=zeros(Niter,1);
            acpt=0; % overall acceptance
            accp=0; % online acceptance
            % run MCMC to collect samples
            fprintf('Running %s ...\n',obj.alg);
            prog=0.05:0.05:1;
            tic;
            for iter=1:Niter

                % display sampleing progress and online acceptance rate
                if ismember(iter,floor(Niter.*prog))
                    fprintf('%.0f%% iterations completed.\n',100*iter/Niter);
                    fprintf('Online acceptance rate: %.0f%%\n', 100*accp/floor(prog(1)*Niter));
                    accp=0;
                end

                % Use MCMC to collect samples
                mcmc=str2func(obj.alg);
                [obj,acpt_ind]=mcmc(obj);
                accp=accp+acpt_ind;

                % burn-in complete
                if(iter==NBurnIn)
                    fprintf('Burn in completed!\n');
                end

                engy(iter) = obj.u;
                % save samples after burn-in
                if(iter>NBurnIn)
                    if mod(iter-NBurnIn,thinning) == 0
                        samp(ceil((iter-NBurnIn)/thinning),:) = obj.q;
                    end
                    acpt = acpt + acpt_ind;
                end
            end
            % count time
            time=toc;
            % save results
            acpt=acpt/(Niter-NBurnIn);
            % save
            time_lbl=regexprep(num2str(fix(clock)),'    ','_');
            f_name=[obj.alg,'_D',num2str(obj.dim),'_',time_lbl];
            if exist('result','dir')~=7
                mkdir('result');
            end
            h=obj.h; L=obj.L;
            save(['result/',f_name,'.mat'],'Nsamp','NBurnIn','thinning','h','L','samp','engy','acpt','time');
        end
    end
end