%%%% - regularizing Ensemble Kalman Filter - %%%%
%%%%   Shiwei Lan                            %%%%
%%%%   CMS, CalTech                          %%%%
%%%%   slan@caltech.edu;                     %%%%
%%%%   Copyright @ 2017                      %%%%
%%%% - regularizing Ensemble Kalman Filter - %%%%

%-- Algorithm 1 of the paper 'A regularizing iterative ensemble Kalman 
%-- method for PDE-constrained inverse problems'
%-- by Marco A Iglesias
%-- Inverse Problems, Volume 32, Number 2, 2016

classdef rEnKF
    properties
        dim; Ne; % dimension of unknown parameter(s) and size of ensemble
        u; % ensemble of state(s)
        ODE; % ODE instance
        G; % (parallel) forward mapping
        data; % data, structure containing data and noise covariance
        nz_lvl=1; % perturbing noise level
    end
    methods
        % constructor
        function obj=rEnKF(u,ODE,G,data,nz_lvl)
            obj.u=u;
            [obj.dim,obj.Ne]=size(u);
            obj.ODE=ODE;
            obj.G=G;
            obj.data=data;
            if exist('nz_lvl','var')
                obj.nz_lvl=nz_lvl;
            end
        end
        
        function [obj,err,u_mean]=update(obj,rho)
            % prediction step
            [w,obj.ODE]=obj.G(obj.u,obj.ODE);
            w_mean=mean(w,2);
            
            % discrepancy principle
            noise=mvnrnd(zeros(1,length(obj.data.y)),obj.nz_lvl^2.*obj.data.Gamma)';
            y_eta=obj.data.y+noise;
            err=sqrt((y_eta-w_mean)'/obj.data.Gamma*(y_eta-w_mean));
            
            % analysis step
            w_tld=w-w_mean;
            C_ww=w_tld*w_tld'./(obj.Ne-1);
            u_mean=mean(obj.u,2);
            C_uw=(obj.u-u_mean)*w_tld'./(obj.Ne-1);
            alpha=1;
            while 1
                alpha=2*alpha;
                err_alpha=(C_ww+alpha.*obj.data.Gamma)\(y_eta-w_mean);
                if alpha*sqrt(err_alpha'*obj.data.Gamma*err_alpha)>=rho*err
                    break;
                end
            end
            d=(C_ww+alpha.*obj.data.Gamma)\(y_eta-w);
            obj.u=obj.u+C_uw*d;
        end
        
        function [u,ensbl,err,time]=run(obj,n_max,rho,tau)
            if ~exist('n_max','var')
                n_max=100;
            end
            if ~exist('rho','var')
                rho=.5; % in (0,1)
            end
            if ~exist('tau','var')
                tau=1/rho; % tau >= 1/rho
            end
            err=zeros(1,n_max);
            tic;
            for n=1:n_max
                % update Kalman fileter
                [obj,err(n),u]=obj.update(rho);
                % record the ensemble
                ensbl=obj.u;
                fprintf(['Unknown parameters: ',repmat('%.4f ',1,min([obj.dim,10])),'\n'],u(1:min([obj.dim,10])));
                fprintf('rEnKF at iteration %d, with error %.8f.\n',n,err(n));
                % terminate if discrepancy principle satisfied
                if err(n)<=tau*obj.nz_lvl
                    break
                end
            end
            % count time
            time=toc;
            fprintf('rEnKF terminates at iteration %d, with error %.4f.\n',n,err(n));
        end
        
    end
end