%%%% - Ensemble Kalman Filter - %%%%
%%%%   Shiwei Lan               %%%%
%%%%   CMS, CalTech             %%%%
%%%%   slan@caltech.edu;        %%%%
%%%%   Copyright @ 2017         %%%%
%%%% - Ensemble Kalman Filter - %%%%

%-- Algorithm 1 of the paper 'Ensemble Kalman methods for inverse problems' 
%-- by Marco A Iglesias, Kody J H Law and Andrew M Stuart
%-- Inverse Problems, Volume 29, Number 4, 2013

classdef EnKF
    properties
        dim; S; % dimension of unknown parameter(s) and size of ensemble
        u; % ensemble of state(s)
        ODE; % ODE instance
        G; % (parallel) forward mapping
        data; % data, structure containing data and noise covariance
        ptb_nz=true; % whether to add perturbing noise
    end
    methods
        % constructor
        function obj=EnKF(u,ODE,G,data,ptb_nz)
            obj.u=u;
            [obj.dim,obj.S]=size(u);
            obj.ODE=ODE;
            obj.G=G;
            obj.data=data;
            if exist('ptb_nz','var')
                obj.ptb_nz=ptb_nz;
            end
        end
        
        function obj=update(obj)
            % prediction step
            [p_hat,obj.ODE]=obj.G(obj.u,obj.ODE);
            p_tld=p_hat-mean(p_hat,2);
            C_pp=p_hat*p_tld'./obj.S;
            C_up=obj.u*p_tld'./obj.S;
            
            % analysis step
            sz_y=length(obj.data.y);
            noise=sparse(sz_y,obj.S);
            if obj.ptb_nz
                noise=mvnrnd(zeros(1,sz_y),obj.data.Gamma,obj.S)';
            end
            y=obj.data.y+noise;
            d=(C_pp+obj.data.Gamma)\(y-p_hat);
            obj.u=obj.u+C_up*d;
        end
        
        function [u_mean,u_std,ensbls,err,time]=run(obj,n_max,thld)
            if ~exist('n_max','var')
                n_max=100;
            end
            if ~exist('thld','var')
                thld=1e-2*sqrt(length(obj.data.y));
            end
            err=zeros(1,n_max);
            u_mean=zeros(obj.dim,n_max);
            u_std=zeros(obj.dim,n_max);
            ensbls=zeros([obj.dim,obj.S,n_max]);
            tic;
            for n=1:n_max
                % update Kalman fileter
                obj=obj.update();
                % update mean of parameter
                ensbl=obj.u;
                u_mean(:,n)=mean(ensbl,2);
                u_std(:,n)=std(ensbl,0,2);
                ensbls(:,:,n)=ensbl;
                fprintf(['Unknown parameters: ',repmat('%.4f ',1,min([obj.dim,10])),'\n'],u_mean(1:min([obj.dim,10]),n));
                [p,obj.ODE]=obj.G(u_mean(:,n),obj.ODE);
                err(n)=sqrt((obj.data.y-p)'/obj.data.Gamma*(obj.data.y-p));
                fprintf('EnKF at iteration %d, with error %.8f.\n',n,err(n));
                if err(n)<=thld
                    break
                end
            end
            % count time
            time=toc;
            fprintf('EnKF terminates at iteration %d, with error %.4f.\n',n,err(n));
        end
        
    end
end